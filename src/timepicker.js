"use strict";

import { h } from './helpers.js';

var chromedTimepicker = {
	predefSettings: {
		original: {
			preserveFocusOnDropdown: true,
			clockBehavior: 'original',
			cursorStyle: 'original'
		},
		recommended: {
			preserveFocusOnDropdown: false,
			clockBehavior: 'alternate',
			cursorStyle: 'alternative'
		}
	}
};

chromedTimepicker.chromedTimepicker = function(valueInput, options = {})
{
	var html = h.getHtmlElement();

	// Configuración por defecto
	const defaultConfig = {
		// Valores
		theme: 'original',
		optionsAmount: 7,
		preserveFocusOnDropdown: false,
		clockBehavior: 'original', // original | alternate
		hours: 24,
		minutes: 60,
		cursorStyle: 'alternative', // original | alternative
		// Eventos
		onChange: (evt) => {},
		onFocus: (evt) => {},
	};

	var config = {...defaultConfig};
	Object.keys(options).forEach(option => {
		const val = options[option];
		if (val !== null) {
			config[option] = val;
		}
	});

	valueInput.setAttribute('type', "hidden");

	var parts = {
		hour: { name: 'hour', limit: config.hours,   start: 0, end: 2, current: () => new Date().getHours() },
		mins: { name: 'mins', limit: config.minutes, start: 3, end: 5, current: () => new Date().getMinutes() },
	};

	var mainContainer = h.createElement('span', {
		class: ['chromed-timepicker', `${config.theme}-theme`]
	});

	var timePickerInput = h.createElement('input', {
		class: ['time-input'],
		attr:  {type: 'text'},
		value: valueInput.value,
	});

	var inputContainer = h.createElement('span', {
		class: ['contenedor-input', `cursor-style-${config.cursorStyle}`]
	});

	h.insertAfter(mainContainer, valueInput);
	mainContainer.appendChild(inputContainer);
	inputContainer.appendChild(timePickerInput);
	mainContainer.appendChild(valueInput);

	var t = timePickerInput;

	var clock = createClockBtnElement();
	inputContainer.appendChild(clock);

	function getOptionsContainers() {
		return Array.from(mainContainer.children)
			.filter(elem => elem.classList.contains('options-containers')).shift();
	}

	function dropdownShown() {
		return !!getOptionsContainers();
	}

	function removeDropdown() {
		const optionsContainers = getOptionsContainers();
		optionsContainers && optionsContainers.remove();
	}

	function showUp() {
		const elemTop = h.getOffsetTop(t);
		const wndwTop = window.scrollY;
		const topDiff = elemTop - wndwTop;
		const elemBottom = h.getOffsetTop(t) + t.offsetHeight;
		const wndwBottom = window.scrollY + window.innerHeight;
		const bottomDiff = wndwBottom - elemBottom;
		const estimatedHeight = 262; // HC // TODO: Medir de alguna forma!!!

		return elemTop > wndwTop // El elemento está al menos visible o abajo de la pantalla
			&& bottomDiff < estimatedHeight // El desplegable no entra abajo
			&& bottomDiff < topDiff; // Arriba hay más lugar para mostrar
	}

	var initialState = {
		hour: {value: null, offset: null, selected: null},
		mins: {value: null, offset: null, selected: null},
	};

	// Evento compartido independiente de qué sección de opciones esté activa
	html.addEventListener('keydown', (evt) => {
		const key = evt.key;
		if (key === 'Escape' && dropdownShown()) {
			// Si no cambio el valor de la hora aún, cierra las opciones
			const currentState = getCurrentState();
			if (currentState.hour.value === initialState.hour.value
			 && currentState.mins.value === initialState.mins.value) {
				return removeDropdown();
			}
			// Si ya cambio, restablece el valor original antes del cambio
			Array.from(getOptionsContainers().children).forEach(optionContainer =>
			{
				const timePart = optionContainer.dataset.timePart;
				const initialState = initialState[timePart];

				setTimePart(timePart, initialState.value);
				// TODO: Este scroll podría solucionarse si la función markValue aceptara un
				//       parámetro para forzar el mostrarlo arriba
				parts[timePart].listContainer.scrollTop = initialState.offset; 
				parts[timePart].markValue(initialState.selected);
			});
		}
	});

	function getCurrentState() {
		var result = {};
		Array.from(getOptionsContainers().children).forEach(optionContainer => {
			const timePart = optionContainer.dataset.timePart;
			result[timePart] = {
				value: getTimePart(timePart),
				offset: parts[timePart].listContainer.scrollTop,
				selected: getTimePartOrCurrent(timePart)
			};
		});
		return result;
	}

	function showDropdown()
	{
		function numberElem(val, clase) {
			const strNum = numToStr(val);
			var li = h.createElement('li', {
				class: ['numero', clase],
				data: { value: strNum }
			});
			li.innerText = strNum;
			return li;
		}
		function partialList(n, clase) {
			var result = [];
			for (var i = 0; i < n; i++) {
				result.push(numberElem(i, clase));
			}
			return result;
		}
		function completeList(n) {
			return ['pre', 'original', 'post']
				.map(clase => partialList(n, clase))
				.flat();
		}

		var optionsContainers = h.createElement('div', {
			class: [
				'options-containers', 
				showUp() ? 'show-on-top' : null,
			],
		});

		h.insertAfter(optionsContainers, inputContainer);

		Object.values(parts).forEach(option => {
			var container = h.createElement('div', {
				class: [
					"options-container",
					`${option.name}-container`
				],
				data: {
					limit: option.limit,
					timePart: option.name,
				}
			});
			optionsContainers.appendChild(container);
		});

		var activeDropdownPart = null;

		function setValueFromDropdown() {
			Object.values(parts).forEach(part => part.setPartFromDropdown());
		}

		function activateDropdown(partName) {
			activeDropdownPart = partName;
			Array.from(optionsContainers.children).forEach(cont => {
				if (cont.classList.contains(`${partName}-container`)) {
					cont.classList.add('active-container');
				} else {
					cont.classList.remove('active-container');
				}
			});
		}

		Array.from(optionsContainers.children).forEach(optionContainer =>
		{
			var listContainer = h.createElement('div');

			const timePart = optionContainer.dataset.timePart;
			const limit = parseInt(optionContainer.dataset.limit);

			parts[timePart].setPartFromDropdown = () => {
				const selected = Array.from(listContainer.children[0].children)
					.filter(elem => elem.classList.contains('selected'));
				if (selected.length) {
					setTimePart(timePart, selected[0].dataset.value);
				}
			};

			parts[timePart].listContainer = listContainer;

			listContainer.classList.add("list-container");
			var ul = h.createElement('ul');
			completeList(optionContainer.dataset.limit).forEach(li => {
				ul.appendChild(li);
			});
			listContainer.appendChild(ul);
			optionContainer.appendChild(listContainer);

			const values = Array.from(listContainer.children[0].children)
				.filter(elem => elem.classList.contains('numero'));
			const n = optionContainer.dataset.limit; // TODO: Falta aplicar lo del step
			const hgt = values[0].offsetHeight;
			const hgtT = hgt * n;

			optionContainer.style.height = (hgt * config.optionsAmount + 3) + 'px'; // HC según margen
			listContainer.style.height   = (hgt * config.optionsAmount + 2) + 'px'; // HC según margen

			listContainer.scrollTop = hgtT;

			function adjustPartialList() {
				if (listContainer.scrollTop + listContainer.offsetHeight > hgtT * 3 - hgt) {
					listContainer.scrollTop -= hgtT;
				}
				if (listContainer.scrollTop < hgt) {
					listContainer.scrollTop += hgtT;
				}
			}

			function correctAlignment() {
				listContainer.scrollTop = Math.round(listContainer.scrollTop / hgt) * hgt;
			}

			function markValueOnTop(value) {
				listContainer.scrollTop = hgt * value;
				markValue(value);
			}

			function markValue(value) 
			{
				values.forEach(elemSel => {
					const valComp = elemSel.dataset.value;
					elemSel.classList[valComp === value ? 'add' : 'remove']('selected');
				});

				const selectedElems = Array.from(listContainer.children[0].children)
					.filter(elem => elem.classList.contains('selected'));

				var nearness = {};
				selectedElems.forEach(elem => {
					const distance = Math.min(
						Math.abs(elem.offsetTop - listContainer.scrollTop),
						Math.abs(elem.offsetHeight + elem.offsetTop - listContainer.scrollTop - listContainer.offsetHeight)
					);
					nearness[distance] = elem;
				});

				const topSpace = parseInt(window.getComputedStyle(listContainer, null)['padding-top']);
				const bottomSpace = parseInt(window.getComputedStyle(listContainer, null)['padding-bottom']) + 2; // HC: Salió de prueba y error
				
				// Evitar que se desborde de la zona visible
				const selected = nearness[Math.min(...Object.keys(nearness))];
				if (selected) {
					// Si está corrido hacia arriba
					if (selected.offsetTop < listContainer.scrollTop) {
						listContainer.scrollTop = selected.offsetTop - topSpace;
					}
					// Si está corrido hacia abajo
					else if (selected.offsetHeight + selected.offsetTop > listContainer.scrollTop + listContainer.offsetHeight) {
						listContainer.scrollTop = selected.offsetHeight + selected.offsetTop - listContainer.offsetHeight + bottomSpace;
					}
				}
			}

			parts[timePart].markValue = markValue;

			var correctAlignmentTimeout;
			listContainer.addEventListener('scroll', (evt) => {
				// Desfasje de vuelta
				adjustPartialList();
				// Estabiliza alineación
				clearTimeout(correctAlignmentTimeout);
				correctAlignmentTimeout = setTimeout(correctAlignment, 500);
			});

			html.addEventListener('keydown', (evt) => {
				if (['F5', 'Alt'].includes(evt.key)) {
					return;
				}
				const key = evt.key;
				if (dropdownShown() && activeDropdownPart === timePart) {
					evt.preventDefault();	
					if (evt.key === 'Enter') {
						removeDropdown();
					}
					if (evt.key === 'ArrowRight') {
						activateDropdown('mins');
					}
					if (evt.key === 'ArrowLeft') {
						activateDropdown('hour');
					}
					if (evt.key === 'Tab') {
						// OBS: El timeout es para prevenir que procese los eventos de las 2 partes
						// y termine en el mismo lugar (hora > minuto > hora)
						// TODO: Resolver de una forma mejor
						setTimeout(() => {
							activateDropdown(activeDropdownPart === 'hour' ? 'mins' : 'hour');
						});
					}

					if (['ArrowUp', 'ArrowDown'].includes(key)) {
						const step = evt.key === 'ArrowUp' ? -1 : 1;
						const newSubVal = numToStr(
							(parseInt(getTimePartOrCurrent(timePart)) + step + limit) % limit
						);
						markValue(newSubVal);
						// setValueFromDropdown();
					}
					if (['ArrowUp', 'ArrowDown', 'Enter'].includes(key)) {
						setValueFromDropdown();
					}
				}
			});

			values.forEach(elem => {

				elem.addEventListener('mousedown', (evt) => {
					if (config.preserveFocusOnDropdown) {
						previousToSlider = document.activeElement;
					}
				});

				elem.addEventListener('click', (evt) => {
					const val = evt.target.dataset.value;
					const [start, end] = [t.selectionStart, t.selectionEnd];

					setTimePart(timePart, val);
					activateDropdown(timePart);
					markValue(val);
					if (config.preserveFocusOnDropdown) {
						if (previousToSlider === t) {
							setTimeout(() => {
								t.focus();
								t.setSelectionRange(start, end);
							});
						}
					}
				});
			});

			markValueOnTop(getTimePartOrCurrent(timePart));
			activateDropdown('hour');
		});

		initialState = getCurrentState();
	}

	html.addEventListener('click', (evt) => {
		if (!preventGlobalClick && dropdownShown()) {
			const listContainerClicked = h.getEventPath(evt).find(elem => { 
				return elem.classList 
					&& elem.classList.contains('list-container'); 
			});
			if (!listContainerClicked) {
				removeDropdown();
			}
		}
		preventGlobalClick = false;
	});

	init();


	/*
	|------------------------------------------------------
	| Clock
	|------------------------------------------------------
	*/

	function createClockBtnElement() {
		var result = h.createElement('span', { class: 'clock-btn'});

		var svg = h.createSvgElement('svg', {
			'viewBox': '0 0 100 100',
			'xmlns': 'http://www.w3.org/2000/svg',
		});

		var circle = h.createSvgElement('circle', {
			'cx': '50', 'cy': '50', 'r': '45',
			'fill-opacity': '0', 'stroke': 'black', 'stroke-width': '10',
		});

		var line1 = h.createSvgElement('line', {
			'x1': '50', 'y1': '55', 'x2': '50', 'y2': '25',
			'stroke': 'black', 'stroke-width': '7.5',
		});

		var line2 = h.createSvgElement('line', {
			'x1': '48', 'y1': '52', 'x2': '74', 'y2': '67',
			'stroke': 'black', 'stroke-width': '7.5',
		});

		result.appendChild(svg);
		svg.appendChild(circle);
		svg.appendChild(line1);
		svg.appendChild(line2);

		return result;
	}

	// OBS: Ubicación provisoria!!!
	var start, end;
	
	var preventGlobalClick = false;

	clock.addEventListener('mousedown', (evt) => {
		if (config.preserveFocusOnDropdown) {
			previousToSlider = document.activeElement;
			[start, end] = [t.selectionStart, t.selectionEnd];
		}
		if (config.clockBehavior === 'original') removeDropdown();
	});
	clock.addEventListener('click', (evt) => {
		if (!dropdownShown()) {
			if (config.preserveFocusOnDropdown) {
				t.focus();
				if (previousToSlider === t) {
					t.setSelectionRange(start, end);
				}
			}
			preventGlobalClick = true;
			showDropdown();
		} else {
			if (config.clockBehavior === 'original') showDropdown();
			if (config.clockBehavior === 'alternate') removeDropdown();
		}
	});


	/*
	|------------------------------------------------------
	| Input
	|------------------------------------------------------
	*/

	function init() {
		if (!t.value) {	
			t.value = "--:--";
		}
	}

	function getTimePart(partName) {
		return t.value.substring(parts[partName].start, parts[partName].end);
	}
	function getHour() { return getTimePart('hour'); }
	function getMins() { return getTimePart('mins'); }

	function getCurrentVal(partName) {
		return parts[partName].current.call(null);
	}
	function getCurrentHour() { return getCurrentVal('hour'); }
	function getCurrentMins() { return getCurrentVal('mins'); }

	function getTimePartOrCurrent(partName) {
		const incompleteValue = Object.keys(parts)
			.filter(partName => isNaN(parseInt(getTimePart(partName))))
			.length > 0;

		return incompleteValue 
			? numToStr(getCurrentVal(partName)) 
			: getTimePart(partName);
	}
	function getHourOrCurrent() { return getTimePartOrCurrent('hour'); }
	function getMinsOrCurrent() { return getTimePartOrCurrent('mins'); }

	function selectTimePart(partName) {
		t.setSelectionRange(parts[partName].start, parts[partName].end);
	}
	function selectHour() { return selectTimePart('hour'); }
	function selectMins() { return selectTimePart('mins'); }

	function focusTimePart(partName) {
		firstDigit = true;
		focusedIn = partName;
		selectTimePart(partName);
	}
	function focusHour() { return focusTimePart('hour'); }
	function focusMins() { return focusTimePart('mins'); }

	function numToStr(val, n = 2) {
		return String(val).substring(val.length - n).padStart(n, '0');
	}

	function setTimePart(partName, val) {
		const [start, end] = [parts[partName].start, parts[partName].end];
		t.value = t.value.substring(0, start)
						+ numToStr(val)
						+ t.value.substring(end);
		selectTimePart(partName);

		valueInput.value = getRealValue(t.value);
		h.triggerEvent(valueInput, 'change');
	}
	function setHour(val) { setTimePart('hour', val);	}
	function setMins(val) { setTimePart('mins', val);	}

	function getRealValue(value) {
		return value.includes('-') ? null : value;
	}

	var firstDigit = false;
	var focusedIn = null;
	var previousToInput = null;
	var previousToSlider = null;

	// Deshabilita pegado y arrastre
	['paste', 'dragstart', 'drop'].forEach((evtName) => {
		t.addEventListener(evtName, (evt) => { evt.preventDefault(); });
	});

	t.addEventListener('focus', (evt) => {
		evt.preventDefault();
		focusHour();
	});
	t.addEventListener('focus', config.onFocus);

	t.addEventListener('focusout', (evt) => {
		// Corrige exceso de minutos
		var mins = getMins();
		if (!isNaN(mins) && mins >= config.minutes) {
			setMins(config.minutes - 1);
		}
	});
	t.addEventListener('focusout', config.onBlur);

	t.addEventListener('mousedown', (evt) => {
		previousToInput = document.activeElement;
	});

	t.addEventListener('keydown', (evt) => {
		// Teclas con funcionamiento normal
		if (['F5', 'Alt'].includes(evt.key)) { return; }

		const timePart = t.selectionStart === 0 ? 'hour' : 'mins';

		// Caso particular del Tab, funciona normal o no según el caso
		if (evt.key === 'Tab' && !evt.shiftKey && timePart === 'mins') { return; }
		if (evt.key === 'Tab' && evt.shiftKey && timePart === 'hour') { return; }

		evt.preventDefault(); // console.log(evt.key);

		// Cancela toda acción para estas teclas si está abierto el menú
		if (dropdownShown()) {
			if (['ArrowRight', 'ArrowLeft', 'ArrowUp', 'ArrowDown', 'Enter'].includes(evt.key)) {
				return;
			}
		}

		if (evt.key === 'Tab' && evt.shiftKey && timePart === 'mins') { 
			return focusHour();
		}
		if (evt.key === 'Tab' && !evt.shiftKey && timePart === 'hour') { 
			return focusMins();
		}

		if (evt.key === 'ArrowRight') {
			return focusMins();
		}
		if (evt.key === 'ArrowLeft') {
			return focusHour();
		}

		const value = t.value;
		const oldSubVal = getTimePart(timePart);
		const limit = focusedIn === 'hour' ? config.hours : config.minutes;
		let newSubVal = '';

		if (['ArrowUp', 'ArrowDown'].includes(evt.key)) {
			const step = evt.key === 'ArrowUp' ? 1 : -1;

			newSubVal = numToStr(
				isNaN(parseInt(oldSubVal))
					? 0 : (parseInt(oldSubVal) + step + limit) % limit
			);
		}

		if (['Backspace', 'Delete'].includes(evt.key)) {
			newSubVal = '--';
			if (focusedIn === 'hour') {
				focusHour();
			}
		}

		if (evt.key.match(/^[0-9]$/))
		{
			newSubVal = (firstDigit ? '0' : oldSubVal.substring(1)) + evt.key;

			if (focusedIn === 'hour') {
				// Excedido en la decena
				if (firstDigit && parseInt(newSubVal) * 10 >= limit) {
					setTimeout(focusMins);
				}
				if (!firstDigit) {
					// Excedido en la unidad
					if (parseInt(newSubVal) >= limit) {
						newSubVal = String(limit - 1);
					}
					setTimeout(focusMins);
				}
			}
			firstDigit = false;
		}

		if (newSubVal) {
			setTimePart(timePart, newSubVal);
		}
	});

	t.addEventListener('mouseup', (evt) => {
		evt.preventDefault();
		if (t.selectionStart >= 3) {
			focusMins();
		} else {
			focusHour();
		}
	});

	valueInput.addEventListener('change', config.onChange);

	return {
		domContainer: mainContainer,
		focus: () => { timePickerInput.focus(); }
	};
};

export default chromedTimepicker;
