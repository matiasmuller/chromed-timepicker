//
// Utilidades generales
//

function insertAfter(newNode, referenceNode) {
	referenceNode.parentNode.insertBefore(newNode, referenceNode.nextSibling);
}

function getOffsetTop(element) {
	if (!element) return 0;
	return getOffsetTop(element.offsetParent) + element.offsetTop;
}

function triggerEvent(element, evtName) {
	if ("createEvent" in document) {
	    var evt = document.createEvent("HTMLEvents");
	    evt.initEvent(evtName, false, true);
	    element.dispatchEvent(evt);
	} else {
	    element.fireEvent("on" + evtName);
	}
}

function getEventPath(event) {
	return event.path || (event.composedPath && event.composedPath());
}

function getBodyElement() {
	return document.getElementsByTagName('body')[0];
}

function getHtmlElement() {
	return document.getElementsByTagName('html')[0];
}

function createElement(tagname, params = {})
{
	var result = document.createElement(tagname);

	if ('class' in params) {
		var classes = params.class || [];
		if (!Array.isArray(classes)) classes = [classes];
		classes.forEach(c => { c && result.classList.add(c); });
	}
	if ('attr' in params) {
		var attributes = params.attr || {};
		Object.keys(attributes).forEach(name => { 
			const value = attributes[name];
			result.setAttribute(name, value);
		});
	}
	if ('data' in params) {
		var data = params.data || {};
		Object.keys(data).forEach(name => { 
			const value = data[name];
			result.dataset[name] = value;
		});
	}
	if ('value' in params) {
		result.value = params.value;
	}

	return result;
}

function createSvgElement(tagname, attributes = {})
{
	var result = document.createElementNS("http://www.w3.org/2000/svg", tagname);

	Object.keys(attributes).forEach(name => { 
		const value = attributes[name];
		result.setAttribute(name, value);
	});

	return result;
}

export var h = {
	insertAfter,
	getOffsetTop,
	triggerEvent,
	getEventPath,
	getBodyElement,
	getHtmlElement,
	createElement,
	createSvgElement,
};