const gulp = require('gulp');
const sass = require('gulp-sass')(require('sass'));
const uglify = require('gulp-uglify');
const rename = require('gulp-rename');
var sourcemaps = require('gulp-sourcemaps');
const pipeline = require('readable-stream').pipeline;
const babelify = require('babelify');
var babel = require('rollup-plugin-babel');
const rollup = require('gulp-better-rollup');
var header = require('gulp-header');
const fs = require('fs');

const distFolder = './dist';
const license_comment = fs.readFileSync('./license_comment', 'utf8');

const _sass = function() {
  gulp.src('./src/*.scss')
    .pipe(sourcemaps.init())
    .pipe(sass().on('error', sass.logError))
    .pipe(sourcemaps.write())
    .pipe(header(license_comment))
    .pipe(gulp.dest(distFolder));
};

const _sassmin = function() {
  gulp.src('./src/*.scss')
    .pipe(sourcemaps.init())
    .pipe(sass({outputStyle: 'compressed'}).on('error', sass.logError))
    .pipe(rename({ suffix: '.min' }))
    .pipe(sourcemaps.write())
    .pipe(header(license_comment))
    .pipe(gulp.dest(distFolder));
};

const _js = () => {
  return gulp.src('src/timepicker.js')
    .pipe(sourcemaps.init())
    .pipe(rollup({
      plugins: [babel({babelrc: false, presets: ['@babel/preset-env']})]
    }, {
      format: 'iife',
      name: 'chromedTimepicker',
    }))
    .pipe(sourcemaps.write()) // inlining the sourcemap into the exported .js file
    .pipe(header(license_comment))
    .pipe(gulp.dest(distFolder));
};

const _jsmin = () => {
  return gulp.src('src/timepicker.js')
    .pipe(sourcemaps.init())
    .pipe(rollup({
      plugins: [babel({babelrc: false, presets: ['@babel/preset-env']})]
    }, {
      format: 'iife',
      name: 'chromedTimepicker',
    }))
    .pipe(uglify({output: {comments: '/^!/'}}))
    .pipe(rename({ suffix: '.min' }))
    .pipe(sourcemaps.write()) // inlining the sourcemap into the exported .js file
    .pipe(header(license_comment))
    .pipe(gulp.dest(distFolder));
};

gulp.task('default', function() {
  _js();
  _jsmin();
  _sass();
  _sassmin();
  gulp.watch('./src/*.js', _js);
  gulp.watch('./src/*.js', _jsmin);
  gulp.watch('./src/*.scss', _sass);
  gulp.watch('./src/*.scss', _sassmin);
});