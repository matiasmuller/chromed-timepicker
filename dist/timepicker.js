/*! chromedTimepicker v0.9 */
var chromedTimepicker = (function () {
  'use strict';

  function ownKeys(object, enumerableOnly) {
    var keys = Object.keys(object);

    if (Object.getOwnPropertySymbols) {
      var symbols = Object.getOwnPropertySymbols(object);
      enumerableOnly && (symbols = symbols.filter(function (sym) {
        return Object.getOwnPropertyDescriptor(object, sym).enumerable;
      })), keys.push.apply(keys, symbols);
    }

    return keys;
  }

  function _objectSpread2(target) {
    for (var i = 1; i < arguments.length; i++) {
      var source = null != arguments[i] ? arguments[i] : {};
      i % 2 ? ownKeys(Object(source), !0).forEach(function (key) {
        _defineProperty(target, key, source[key]);
      }) : Object.getOwnPropertyDescriptors ? Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)) : ownKeys(Object(source)).forEach(function (key) {
        Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key));
      });
    }

    return target;
  }

  function _defineProperty(obj, key, value) {
    if (key in obj) {
      Object.defineProperty(obj, key, {
        value: value,
        enumerable: true,
        configurable: true,
        writable: true
      });
    } else {
      obj[key] = value;
    }

    return obj;
  }

  function _toConsumableArray(arr) {
    return _arrayWithoutHoles(arr) || _iterableToArray(arr) || _unsupportedIterableToArray(arr) || _nonIterableSpread();
  }

  function _arrayWithoutHoles(arr) {
    if (Array.isArray(arr)) return _arrayLikeToArray(arr);
  }

  function _iterableToArray(iter) {
    if (typeof Symbol !== "undefined" && iter[Symbol.iterator] != null || iter["@@iterator"] != null) return Array.from(iter);
  }

  function _unsupportedIterableToArray(o, minLen) {
    if (!o) return;
    if (typeof o === "string") return _arrayLikeToArray(o, minLen);
    var n = Object.prototype.toString.call(o).slice(8, -1);
    if (n === "Object" && o.constructor) n = o.constructor.name;
    if (n === "Map" || n === "Set") return Array.from(o);
    if (n === "Arguments" || /^(?:Ui|I)nt(?:8|16|32)(?:Clamped)?Array$/.test(n)) return _arrayLikeToArray(o, minLen);
  }

  function _arrayLikeToArray(arr, len) {
    if (len == null || len > arr.length) len = arr.length;

    for (var i = 0, arr2 = new Array(len); i < len; i++) arr2[i] = arr[i];

    return arr2;
  }

  function _nonIterableSpread() {
    throw new TypeError("Invalid attempt to spread non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method.");
  }

  //
  // Utilidades generales
  //
  function insertAfter(newNode, referenceNode) {
    referenceNode.parentNode.insertBefore(newNode, referenceNode.nextSibling);
  }

  function getOffsetTop(element) {
    if (!element) return 0;
    return getOffsetTop(element.offsetParent) + element.offsetTop;
  }

  function triggerEvent(element, evtName) {
    if ("createEvent" in document) {
      var evt = document.createEvent("HTMLEvents");
      evt.initEvent(evtName, false, true);
      element.dispatchEvent(evt);
    } else {
      element.fireEvent("on" + evtName);
    }
  }

  function getEventPath(event) {
    return event.path || event.composedPath && event.composedPath();
  }

  function getBodyElement() {
    return document.getElementsByTagName('body')[0];
  }

  function getHtmlElement() {
    return document.getElementsByTagName('html')[0];
  }

  function createElement(tagname) {
    var params = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : {};
    var result = document.createElement(tagname);

    if ('class' in params) {
      var classes = params["class"] || [];
      if (!Array.isArray(classes)) classes = [classes];
      classes.forEach(function (c) {
        c && result.classList.add(c);
      });
    }

    if ('attr' in params) {
      var attributes = params.attr || {};
      Object.keys(attributes).forEach(function (name) {
        var value = attributes[name];
        result.setAttribute(name, value);
      });
    }

    if ('data' in params) {
      var data = params.data || {};
      Object.keys(data).forEach(function (name) {
        var value = data[name];
        result.dataset[name] = value;
      });
    }

    if ('value' in params) {
      result.value = params.value;
    }

    return result;
  }

  function createSvgElement(tagname) {
    var attributes = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : {};
    var result = document.createElementNS("http://www.w3.org/2000/svg", tagname);
    Object.keys(attributes).forEach(function (name) {
      var value = attributes[name];
      result.setAttribute(name, value);
    });
    return result;
  }

  var h = {
    insertAfter: insertAfter,
    getOffsetTop: getOffsetTop,
    triggerEvent: triggerEvent,
    getEventPath: getEventPath,
    getBodyElement: getBodyElement,
    getHtmlElement: getHtmlElement,
    createElement: createElement,
    createSvgElement: createSvgElement
  };

  var chromedTimepicker = {
    predefSettings: {
      original: {
        preserveFocusOnDropdown: true,
        clockBehavior: 'original',
        cursorStyle: 'original'
      },
      recommended: {
        preserveFocusOnDropdown: false,
        clockBehavior: 'alternate',
        cursorStyle: 'alternative'
      }
    }
  };

  chromedTimepicker.chromedTimepicker = function (valueInput) {
    var options = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : {};
    var html = h.getHtmlElement(); // Configuración por defecto

    var defaultConfig = {
      // Valores
      theme: 'original',
      optionsAmount: 7,
      preserveFocusOnDropdown: false,
      clockBehavior: 'original',
      // original | alternate
      hours: 24,
      minutes: 60,
      cursorStyle: 'alternative',
      // original | alternative
      // Eventos
      onChange: function onChange(evt) {},
      onFocus: function onFocus(evt) {}
    };

    var config = _objectSpread2({}, defaultConfig);

    Object.keys(options).forEach(function (option) {
      var val = options[option];

      if (val !== null) {
        config[option] = val;
      }
    });
    valueInput.setAttribute('type', "hidden");
    var parts = {
      hour: {
        name: 'hour',
        limit: config.hours,
        start: 0,
        end: 2,
        current: function current() {
          return new Date().getHours();
        }
      },
      mins: {
        name: 'mins',
        limit: config.minutes,
        start: 3,
        end: 5,
        current: function current() {
          return new Date().getMinutes();
        }
      }
    };
    var mainContainer = h.createElement('span', {
      "class": ['chromed-timepicker', "".concat(config.theme, "-theme")]
    });
    var timePickerInput = h.createElement('input', {
      "class": ['time-input'],
      attr: {
        type: 'text'
      },
      value: valueInput.value
    });
    var inputContainer = h.createElement('span', {
      "class": ['contenedor-input', "cursor-style-".concat(config.cursorStyle)]
    });
    h.insertAfter(mainContainer, valueInput);
    mainContainer.appendChild(inputContainer);
    inputContainer.appendChild(timePickerInput);
    mainContainer.appendChild(valueInput);
    var t = timePickerInput;
    var clock = createClockBtnElement();
    inputContainer.appendChild(clock);

    function getOptionsContainers() {
      return Array.from(mainContainer.children).filter(function (elem) {
        return elem.classList.contains('options-containers');
      }).shift();
    }

    function dropdownShown() {
      return !!getOptionsContainers();
    }

    function removeDropdown() {
      var optionsContainers = getOptionsContainers();
      optionsContainers && optionsContainers.remove();
    }

    function showUp() {
      var elemTop = h.getOffsetTop(t);
      var wndwTop = window.scrollY;
      var topDiff = elemTop - wndwTop;
      var elemBottom = h.getOffsetTop(t) + t.offsetHeight;
      var wndwBottom = window.scrollY + window.innerHeight;
      var bottomDiff = wndwBottom - elemBottom;
      var estimatedHeight = 262; // HC // TODO: Medir de alguna forma!!!

      return elemTop > wndwTop // El elemento está al menos visible o abajo de la pantalla
      && bottomDiff < estimatedHeight // El desplegable no entra abajo
      && bottomDiff < topDiff; // Arriba hay más lugar para mostrar
    }

    var initialState = {
      hour: {
        value: null,
        offset: null,
        selected: null
      },
      mins: {
        value: null,
        offset: null,
        selected: null
      }
    }; // Evento compartido independiente de qué sección de opciones esté activa

    html.addEventListener('keydown', function (evt) {
      var key = evt.key;

      if (key === 'Escape' && dropdownShown()) {
        // Si no cambio el valor de la hora aún, cierra las opciones
        var currentState = getCurrentState();

        if (currentState.hour.value === initialState.hour.value && currentState.mins.value === initialState.mins.value) {
          return removeDropdown();
        } // Si ya cambio, restablece el valor original antes del cambio


        Array.from(getOptionsContainers().children).forEach(function (optionContainer) {
          var timePart = optionContainer.dataset.timePart;
          var initialState = initialState[timePart];
          setTimePart(timePart, initialState.value); // TODO: Este scroll podría solucionarse si la función markValue aceptara un
          //       parámetro para forzar el mostrarlo arriba

          parts[timePart].listContainer.scrollTop = initialState.offset;
          parts[timePart].markValue(initialState.selected);
        });
      }
    });

    function getCurrentState() {
      var result = {};
      Array.from(getOptionsContainers().children).forEach(function (optionContainer) {
        var timePart = optionContainer.dataset.timePart;
        result[timePart] = {
          value: getTimePart(timePart),
          offset: parts[timePart].listContainer.scrollTop,
          selected: getTimePartOrCurrent(timePart)
        };
      });
      return result;
    }

    function showDropdown() {
      function numberElem(val, clase) {
        var strNum = numToStr(val);
        var li = h.createElement('li', {
          "class": ['numero', clase],
          data: {
            value: strNum
          }
        });
        li.innerText = strNum;
        return li;
      }

      function partialList(n, clase) {
        var result = [];

        for (var i = 0; i < n; i++) {
          result.push(numberElem(i, clase));
        }

        return result;
      }

      function completeList(n) {
        return ['pre', 'original', 'post'].map(function (clase) {
          return partialList(n, clase);
        }).flat();
      }

      var optionsContainers = h.createElement('div', {
        "class": ['options-containers', showUp() ? 'show-on-top' : null]
      });
      h.insertAfter(optionsContainers, inputContainer);
      Object.values(parts).forEach(function (option) {
        var container = h.createElement('div', {
          "class": ["options-container", "".concat(option.name, "-container")],
          data: {
            limit: option.limit,
            timePart: option.name
          }
        });
        optionsContainers.appendChild(container);
      });
      var activeDropdownPart = null;

      function setValueFromDropdown() {
        Object.values(parts).forEach(function (part) {
          return part.setPartFromDropdown();
        });
      }

      function activateDropdown(partName) {
        activeDropdownPart = partName;
        Array.from(optionsContainers.children).forEach(function (cont) {
          if (cont.classList.contains("".concat(partName, "-container"))) {
            cont.classList.add('active-container');
          } else {
            cont.classList.remove('active-container');
          }
        });
      }

      Array.from(optionsContainers.children).forEach(function (optionContainer) {
        var listContainer = h.createElement('div');
        var timePart = optionContainer.dataset.timePart;
        var limit = parseInt(optionContainer.dataset.limit);

        parts[timePart].setPartFromDropdown = function () {
          var selected = Array.from(listContainer.children[0].children).filter(function (elem) {
            return elem.classList.contains('selected');
          });

          if (selected.length) {
            setTimePart(timePart, selected[0].dataset.value);
          }
        };

        parts[timePart].listContainer = listContainer;
        listContainer.classList.add("list-container");
        var ul = h.createElement('ul');
        completeList(optionContainer.dataset.limit).forEach(function (li) {
          ul.appendChild(li);
        });
        listContainer.appendChild(ul);
        optionContainer.appendChild(listContainer);
        var values = Array.from(listContainer.children[0].children).filter(function (elem) {
          return elem.classList.contains('numero');
        });
        var n = optionContainer.dataset.limit; // TODO: Falta aplicar lo del step

        var hgt = values[0].offsetHeight;
        var hgtT = hgt * n;
        optionContainer.style.height = hgt * config.optionsAmount + 3 + 'px'; // HC según margen

        listContainer.style.height = hgt * config.optionsAmount + 2 + 'px'; // HC según margen

        listContainer.scrollTop = hgtT;

        function adjustPartialList() {
          if (listContainer.scrollTop + listContainer.offsetHeight > hgtT * 3 - hgt) {
            listContainer.scrollTop -= hgtT;
          }

          if (listContainer.scrollTop < hgt) {
            listContainer.scrollTop += hgtT;
          }
        }

        function correctAlignment() {
          listContainer.scrollTop = Math.round(listContainer.scrollTop / hgt) * hgt;
        }

        function markValueOnTop(value) {
          listContainer.scrollTop = hgt * value;
          markValue(value);
        }

        function markValue(value) {
          values.forEach(function (elemSel) {
            var valComp = elemSel.dataset.value;
            elemSel.classList[valComp === value ? 'add' : 'remove']('selected');
          });
          var selectedElems = Array.from(listContainer.children[0].children).filter(function (elem) {
            return elem.classList.contains('selected');
          });
          var nearness = {};
          selectedElems.forEach(function (elem) {
            var distance = Math.min(Math.abs(elem.offsetTop - listContainer.scrollTop), Math.abs(elem.offsetHeight + elem.offsetTop - listContainer.scrollTop - listContainer.offsetHeight));
            nearness[distance] = elem;
          });
          var topSpace = parseInt(window.getComputedStyle(listContainer, null)['padding-top']);
          var bottomSpace = parseInt(window.getComputedStyle(listContainer, null)['padding-bottom']) + 2; // HC: Salió de prueba y error
          // Evitar que se desborde de la zona visible

          var selected = nearness[Math.min.apply(Math, _toConsumableArray(Object.keys(nearness)))];

          if (selected) {
            // Si está corrido hacia arriba
            if (selected.offsetTop < listContainer.scrollTop) {
              listContainer.scrollTop = selected.offsetTop - topSpace;
            } // Si está corrido hacia abajo
            else if (selected.offsetHeight + selected.offsetTop > listContainer.scrollTop + listContainer.offsetHeight) {
              listContainer.scrollTop = selected.offsetHeight + selected.offsetTop - listContainer.offsetHeight + bottomSpace;
            }
          }
        }

        parts[timePart].markValue = markValue;
        var correctAlignmentTimeout;
        listContainer.addEventListener('scroll', function (evt) {
          // Desfasje de vuelta
          adjustPartialList(); // Estabiliza alineación

          clearTimeout(correctAlignmentTimeout);
          correctAlignmentTimeout = setTimeout(correctAlignment, 500);
        });
        html.addEventListener('keydown', function (evt) {
          if (['F5', 'Alt'].includes(evt.key)) {
            return;
          }

          var key = evt.key;

          if (dropdownShown() && activeDropdownPart === timePart) {
            evt.preventDefault();

            if (evt.key === 'Enter') {
              removeDropdown();
            }

            if (evt.key === 'ArrowRight') {
              activateDropdown('mins');
            }

            if (evt.key === 'ArrowLeft') {
              activateDropdown('hour');
            }

            if (evt.key === 'Tab') {
              // OBS: El timeout es para prevenir que procese los eventos de las 2 partes
              // y termine en el mismo lugar (hora > minuto > hora)
              // TODO: Resolver de una forma mejor
              setTimeout(function () {
                activateDropdown(activeDropdownPart === 'hour' ? 'mins' : 'hour');
              });
            }

            if (['ArrowUp', 'ArrowDown'].includes(key)) {
              var step = evt.key === 'ArrowUp' ? -1 : 1;
              var newSubVal = numToStr((parseInt(getTimePartOrCurrent(timePart)) + step + limit) % limit);
              markValue(newSubVal); // setValueFromDropdown();
            }

            if (['ArrowUp', 'ArrowDown', 'Enter'].includes(key)) {
              setValueFromDropdown();
            }
          }
        });
        values.forEach(function (elem) {
          elem.addEventListener('mousedown', function (evt) {
            if (config.preserveFocusOnDropdown) {
              previousToSlider = document.activeElement;
            }
          });
          elem.addEventListener('click', function (evt) {
            var val = evt.target.dataset.value;
            var _ref = [t.selectionStart, t.selectionEnd],
                start = _ref[0],
                end = _ref[1];
            setTimePart(timePart, val);
            activateDropdown(timePart);
            markValue(val);

            if (config.preserveFocusOnDropdown) {
              if (previousToSlider === t) {
                setTimeout(function () {
                  t.focus();
                  t.setSelectionRange(start, end);
                });
              }
            }
          });
        });
        markValueOnTop(getTimePartOrCurrent(timePart));
        activateDropdown('hour');
      });
      initialState = getCurrentState();
    }

    html.addEventListener('click', function (evt) {
      if (!preventGlobalClick && dropdownShown()) {
        var listContainerClicked = h.getEventPath(evt).find(function (elem) {
          return elem.classList && elem.classList.contains('list-container');
        });

        if (!listContainerClicked) {
          removeDropdown();
        }
      }

      preventGlobalClick = false;
    });
    init();
    /*
    |------------------------------------------------------
    | Clock
    |------------------------------------------------------
    */

    function createClockBtnElement() {
      var result = h.createElement('span', {
        "class": 'clock-btn'
      });
      var svg = h.createSvgElement('svg', {
        'viewBox': '0 0 100 100',
        'xmlns': 'http://www.w3.org/2000/svg'
      });
      var circle = h.createSvgElement('circle', {
        'cx': '50',
        'cy': '50',
        'r': '45',
        'fill-opacity': '0',
        'stroke': 'black',
        'stroke-width': '10'
      });
      var line1 = h.createSvgElement('line', {
        'x1': '50',
        'y1': '55',
        'x2': '50',
        'y2': '25',
        'stroke': 'black',
        'stroke-width': '7.5'
      });
      var line2 = h.createSvgElement('line', {
        'x1': '48',
        'y1': '52',
        'x2': '74',
        'y2': '67',
        'stroke': 'black',
        'stroke-width': '7.5'
      });
      result.appendChild(svg);
      svg.appendChild(circle);
      svg.appendChild(line1);
      svg.appendChild(line2);
      return result;
    } // OBS: Ubicación provisoria!!!


    var start, end;
    var preventGlobalClick = false;
    clock.addEventListener('mousedown', function (evt) {
      if (config.preserveFocusOnDropdown) {
        previousToSlider = document.activeElement;
        var _ref2 = [t.selectionStart, t.selectionEnd];
        start = _ref2[0];
        end = _ref2[1];
      }

      if (config.clockBehavior === 'original') removeDropdown();
    });
    clock.addEventListener('click', function (evt) {
      if (!dropdownShown()) {
        if (config.preserveFocusOnDropdown) {
          t.focus();

          if (previousToSlider === t) {
            t.setSelectionRange(start, end);
          }
        }

        preventGlobalClick = true;
        showDropdown();
      } else {
        if (config.clockBehavior === 'original') showDropdown();
        if (config.clockBehavior === 'alternate') removeDropdown();
      }
    });
    /*
    |------------------------------------------------------
    | Input
    |------------------------------------------------------
    */

    function init() {
      if (!t.value) {
        t.value = "--:--";
      }
    }

    function getTimePart(partName) {
      return t.value.substring(parts[partName].start, parts[partName].end);
    }

    function getMins() {
      return getTimePart('mins');
    }

    function getCurrentVal(partName) {
      return parts[partName].current.call(null);
    }

    function getTimePartOrCurrent(partName) {
      var incompleteValue = Object.keys(parts).filter(function (partName) {
        return isNaN(parseInt(getTimePart(partName)));
      }).length > 0;
      return incompleteValue ? numToStr(getCurrentVal(partName)) : getTimePart(partName);
    }

    function selectTimePart(partName) {
      t.setSelectionRange(parts[partName].start, parts[partName].end);
    }

    function focusTimePart(partName) {
      firstDigit = true;
      focusedIn = partName;
      selectTimePart(partName);
    }

    function focusHour() {
      return focusTimePart('hour');
    }

    function focusMins() {
      return focusTimePart('mins');
    }

    function numToStr(val) {
      var n = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : 2;
      return String(val).substring(val.length - n).padStart(n, '0');
    }

    function setTimePart(partName, val) {
      var _ref3 = [parts[partName].start, parts[partName].end],
          start = _ref3[0],
          end = _ref3[1];
      t.value = t.value.substring(0, start) + numToStr(val) + t.value.substring(end);
      selectTimePart(partName);
      valueInput.value = getRealValue(t.value);
      h.triggerEvent(valueInput, 'change');
    }

    function setMins(val) {
      setTimePart('mins', val);
    }

    function getRealValue(value) {
      return value.includes('-') ? null : value;
    }

    var firstDigit = false;
    var focusedIn = null;
    var previousToSlider = null; // Deshabilita pegado y arrastre

    ['paste', 'dragstart', 'drop'].forEach(function (evtName) {
      t.addEventListener(evtName, function (evt) {
        evt.preventDefault();
      });
    });
    t.addEventListener('focus', function (evt) {
      evt.preventDefault();
      focusHour();
    });
    t.addEventListener('focus', config.onFocus);
    t.addEventListener('focusout', function (evt) {
      // Corrige exceso de minutos
      var mins = getMins();

      if (!isNaN(mins) && mins >= config.minutes) {
        setMins(config.minutes - 1);
      }
    });
    t.addEventListener('focusout', config.onBlur);
    t.addEventListener('mousedown', function (evt) {
    });
    t.addEventListener('keydown', function (evt) {
      // Teclas con funcionamiento normal
      if (['F5', 'Alt'].includes(evt.key)) {
        return;
      }

      var timePart = t.selectionStart === 0 ? 'hour' : 'mins'; // Caso particular del Tab, funciona normal o no según el caso

      if (evt.key === 'Tab' && !evt.shiftKey && timePart === 'mins') {
        return;
      }

      if (evt.key === 'Tab' && evt.shiftKey && timePart === 'hour') {
        return;
      }

      evt.preventDefault(); // console.log(evt.key);
      // Cancela toda acción para estas teclas si está abierto el menú

      if (dropdownShown()) {
        if (['ArrowRight', 'ArrowLeft', 'ArrowUp', 'ArrowDown', 'Enter'].includes(evt.key)) {
          return;
        }
      }

      if (evt.key === 'Tab' && evt.shiftKey && timePart === 'mins') {
        return focusHour();
      }

      if (evt.key === 'Tab' && !evt.shiftKey && timePart === 'hour') {
        return focusMins();
      }

      if (evt.key === 'ArrowRight') {
        return focusMins();
      }

      if (evt.key === 'ArrowLeft') {
        return focusHour();
      }

      var value = t.value;
      var oldSubVal = getTimePart(timePart);
      var limit = focusedIn === 'hour' ? config.hours : config.minutes;
      var newSubVal = '';

      if (['ArrowUp', 'ArrowDown'].includes(evt.key)) {
        var step = evt.key === 'ArrowUp' ? 1 : -1;
        newSubVal = numToStr(isNaN(parseInt(oldSubVal)) ? 0 : (parseInt(oldSubVal) + step + limit) % limit);
      }

      if (['Backspace', 'Delete'].includes(evt.key)) {
        newSubVal = '--';

        if (focusedIn === 'hour') {
          focusHour();
        }
      }

      if (evt.key.match(/^[0-9]$/)) {
        newSubVal = (firstDigit ? '0' : oldSubVal.substring(1)) + evt.key;

        if (focusedIn === 'hour') {
          // Excedido en la decena
          if (firstDigit && parseInt(newSubVal) * 10 >= limit) {
            setTimeout(focusMins);
          }

          if (!firstDigit) {
            // Excedido en la unidad
            if (parseInt(newSubVal) >= limit) {
              newSubVal = String(limit - 1);
            }

            setTimeout(focusMins);
          }
        }

        firstDigit = false;
      }

      if (newSubVal) {
        setTimePart(timePart, newSubVal);
      }
    });
    t.addEventListener('mouseup', function (evt) {
      evt.preventDefault();

      if (t.selectionStart >= 3) {
        focusMins();
      } else {
        focusHour();
      }
    });
    valueInput.addEventListener('change', config.onChange);
    return {
      domContainer: mainContainer,
      focus: function focus() {
        timePickerInput.focus();
      }
    };
  };

  return chromedTimepicker;

}());

//# sourceMappingURL=data:application/json;charset=utf8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoidGltZXBpY2tlci5qcyIsInNvdXJjZXMiOlsic3JjL2hlbHBlcnMuanMiLCJzcmMvdGltZXBpY2tlci5qcyJdLCJzb3VyY2VzQ29udGVudCI6WyIvL1xyXG4vLyBVdGlsaWRhZGVzIGdlbmVyYWxlc1xyXG4vL1xyXG5cclxuZnVuY3Rpb24gaW5zZXJ0QWZ0ZXIobmV3Tm9kZSwgcmVmZXJlbmNlTm9kZSkge1xyXG5cdHJlZmVyZW5jZU5vZGUucGFyZW50Tm9kZS5pbnNlcnRCZWZvcmUobmV3Tm9kZSwgcmVmZXJlbmNlTm9kZS5uZXh0U2libGluZyk7XHJcbn1cclxuXHJcbmZ1bmN0aW9uIGdldE9mZnNldFRvcChlbGVtZW50KSB7XHJcblx0aWYgKCFlbGVtZW50KSByZXR1cm4gMDtcclxuXHRyZXR1cm4gZ2V0T2Zmc2V0VG9wKGVsZW1lbnQub2Zmc2V0UGFyZW50KSArIGVsZW1lbnQub2Zmc2V0VG9wO1xyXG59XHJcblxyXG5mdW5jdGlvbiB0cmlnZ2VyRXZlbnQoZWxlbWVudCwgZXZ0TmFtZSkge1xyXG5cdGlmIChcImNyZWF0ZUV2ZW50XCIgaW4gZG9jdW1lbnQpIHtcclxuXHQgICAgdmFyIGV2dCA9IGRvY3VtZW50LmNyZWF0ZUV2ZW50KFwiSFRNTEV2ZW50c1wiKTtcclxuXHQgICAgZXZ0LmluaXRFdmVudChldnROYW1lLCBmYWxzZSwgdHJ1ZSk7XHJcblx0ICAgIGVsZW1lbnQuZGlzcGF0Y2hFdmVudChldnQpO1xyXG5cdH0gZWxzZSB7XHJcblx0ICAgIGVsZW1lbnQuZmlyZUV2ZW50KFwib25cIiArIGV2dE5hbWUpO1xyXG5cdH1cclxufVxyXG5cclxuZnVuY3Rpb24gZ2V0RXZlbnRQYXRoKGV2ZW50KSB7XHJcblx0cmV0dXJuIGV2ZW50LnBhdGggfHwgKGV2ZW50LmNvbXBvc2VkUGF0aCAmJiBldmVudC5jb21wb3NlZFBhdGgoKSk7XHJcbn1cclxuXHJcbmZ1bmN0aW9uIGdldEJvZHlFbGVtZW50KCkge1xyXG5cdHJldHVybiBkb2N1bWVudC5nZXRFbGVtZW50c0J5VGFnTmFtZSgnYm9keScpWzBdO1xyXG59XHJcblxyXG5mdW5jdGlvbiBnZXRIdG1sRWxlbWVudCgpIHtcclxuXHRyZXR1cm4gZG9jdW1lbnQuZ2V0RWxlbWVudHNCeVRhZ05hbWUoJ2h0bWwnKVswXTtcclxufVxyXG5cclxuZnVuY3Rpb24gY3JlYXRlRWxlbWVudCh0YWduYW1lLCBwYXJhbXMgPSB7fSlcclxue1xyXG5cdHZhciByZXN1bHQgPSBkb2N1bWVudC5jcmVhdGVFbGVtZW50KHRhZ25hbWUpO1xyXG5cclxuXHRpZiAoJ2NsYXNzJyBpbiBwYXJhbXMpIHtcclxuXHRcdHZhciBjbGFzc2VzID0gcGFyYW1zLmNsYXNzIHx8IFtdO1xyXG5cdFx0aWYgKCFBcnJheS5pc0FycmF5KGNsYXNzZXMpKSBjbGFzc2VzID0gW2NsYXNzZXNdO1xyXG5cdFx0Y2xhc3Nlcy5mb3JFYWNoKGMgPT4geyBjICYmIHJlc3VsdC5jbGFzc0xpc3QuYWRkKGMpOyB9KTtcclxuXHR9XHJcblx0aWYgKCdhdHRyJyBpbiBwYXJhbXMpIHtcclxuXHRcdHZhciBhdHRyaWJ1dGVzID0gcGFyYW1zLmF0dHIgfHwge307XHJcblx0XHRPYmplY3Qua2V5cyhhdHRyaWJ1dGVzKS5mb3JFYWNoKG5hbWUgPT4geyBcclxuXHRcdFx0Y29uc3QgdmFsdWUgPSBhdHRyaWJ1dGVzW25hbWVdO1xyXG5cdFx0XHRyZXN1bHQuc2V0QXR0cmlidXRlKG5hbWUsIHZhbHVlKTtcclxuXHRcdH0pO1xyXG5cdH1cclxuXHRpZiAoJ2RhdGEnIGluIHBhcmFtcykge1xyXG5cdFx0dmFyIGRhdGEgPSBwYXJhbXMuZGF0YSB8fCB7fTtcclxuXHRcdE9iamVjdC5rZXlzKGRhdGEpLmZvckVhY2gobmFtZSA9PiB7IFxyXG5cdFx0XHRjb25zdCB2YWx1ZSA9IGRhdGFbbmFtZV07XHJcblx0XHRcdHJlc3VsdC5kYXRhc2V0W25hbWVdID0gdmFsdWU7XHJcblx0XHR9KTtcclxuXHR9XHJcblx0aWYgKCd2YWx1ZScgaW4gcGFyYW1zKSB7XHJcblx0XHRyZXN1bHQudmFsdWUgPSBwYXJhbXMudmFsdWU7XHJcblx0fVxyXG5cclxuXHRyZXR1cm4gcmVzdWx0O1xyXG59XHJcblxyXG5mdW5jdGlvbiBjcmVhdGVTdmdFbGVtZW50KHRhZ25hbWUsIGF0dHJpYnV0ZXMgPSB7fSlcclxue1xyXG5cdHZhciByZXN1bHQgPSBkb2N1bWVudC5jcmVhdGVFbGVtZW50TlMoXCJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2Z1wiLCB0YWduYW1lKTtcclxuXHJcblx0T2JqZWN0LmtleXMoYXR0cmlidXRlcykuZm9yRWFjaChuYW1lID0+IHsgXHJcblx0XHRjb25zdCB2YWx1ZSA9IGF0dHJpYnV0ZXNbbmFtZV07XHJcblx0XHRyZXN1bHQuc2V0QXR0cmlidXRlKG5hbWUsIHZhbHVlKTtcclxuXHR9KTtcclxuXHJcblx0cmV0dXJuIHJlc3VsdDtcclxufVxyXG5cclxuZXhwb3J0IHZhciBoID0ge1xyXG5cdGluc2VydEFmdGVyLFxyXG5cdGdldE9mZnNldFRvcCxcclxuXHR0cmlnZ2VyRXZlbnQsXHJcblx0Z2V0RXZlbnRQYXRoLFxyXG5cdGdldEJvZHlFbGVtZW50LFxyXG5cdGdldEh0bWxFbGVtZW50LFxyXG5cdGNyZWF0ZUVsZW1lbnQsXHJcblx0Y3JlYXRlU3ZnRWxlbWVudCxcclxufTsiLCJcInVzZSBzdHJpY3RcIjtcclxuXHJcbmltcG9ydCB7IGggfSBmcm9tICcuL2hlbHBlcnMuanMnO1xyXG5cclxudmFyIGNocm9tZWRUaW1lcGlja2VyID0ge1xyXG5cdHByZWRlZlNldHRpbmdzOiB7XHJcblx0XHRvcmlnaW5hbDoge1xyXG5cdFx0XHRwcmVzZXJ2ZUZvY3VzT25Ecm9wZG93bjogdHJ1ZSxcclxuXHRcdFx0Y2xvY2tCZWhhdmlvcjogJ29yaWdpbmFsJyxcclxuXHRcdFx0Y3Vyc29yU3R5bGU6ICdvcmlnaW5hbCdcclxuXHRcdH0sXHJcblx0XHRyZWNvbW1lbmRlZDoge1xyXG5cdFx0XHRwcmVzZXJ2ZUZvY3VzT25Ecm9wZG93bjogZmFsc2UsXHJcblx0XHRcdGNsb2NrQmVoYXZpb3I6ICdhbHRlcm5hdGUnLFxyXG5cdFx0XHRjdXJzb3JTdHlsZTogJ2FsdGVybmF0aXZlJ1xyXG5cdFx0fVxyXG5cdH1cclxufTtcclxuXHJcbmNocm9tZWRUaW1lcGlja2VyLmNocm9tZWRUaW1lcGlja2VyID0gZnVuY3Rpb24odmFsdWVJbnB1dCwgb3B0aW9ucyA9IHt9KVxyXG57XHJcblx0dmFyIGh0bWwgPSBoLmdldEh0bWxFbGVtZW50KCk7XHJcblxyXG5cdC8vIENvbmZpZ3VyYWNpw7NuIHBvciBkZWZlY3RvXHJcblx0Y29uc3QgZGVmYXVsdENvbmZpZyA9IHtcclxuXHRcdC8vIFZhbG9yZXNcclxuXHRcdHRoZW1lOiAnb3JpZ2luYWwnLFxyXG5cdFx0b3B0aW9uc0Ftb3VudDogNyxcclxuXHRcdHByZXNlcnZlRm9jdXNPbkRyb3Bkb3duOiBmYWxzZSxcclxuXHRcdGNsb2NrQmVoYXZpb3I6ICdvcmlnaW5hbCcsIC8vIG9yaWdpbmFsIHwgYWx0ZXJuYXRlXHJcblx0XHRob3VyczogMjQsXHJcblx0XHRtaW51dGVzOiA2MCxcclxuXHRcdGN1cnNvclN0eWxlOiAnYWx0ZXJuYXRpdmUnLCAvLyBvcmlnaW5hbCB8IGFsdGVybmF0aXZlXHJcblx0XHQvLyBFdmVudG9zXHJcblx0XHRvbkNoYW5nZTogKGV2dCkgPT4ge30sXHJcblx0XHRvbkZvY3VzOiAoZXZ0KSA9PiB7fSxcclxuXHR9O1xyXG5cclxuXHR2YXIgY29uZmlnID0gey4uLmRlZmF1bHRDb25maWd9O1xyXG5cdE9iamVjdC5rZXlzKG9wdGlvbnMpLmZvckVhY2gob3B0aW9uID0+IHtcclxuXHRcdGNvbnN0IHZhbCA9IG9wdGlvbnNbb3B0aW9uXTtcclxuXHRcdGlmICh2YWwgIT09IG51bGwpIHtcclxuXHRcdFx0Y29uZmlnW29wdGlvbl0gPSB2YWw7XHJcblx0XHR9XHJcblx0fSk7XHJcblxyXG5cdHZhbHVlSW5wdXQuc2V0QXR0cmlidXRlKCd0eXBlJywgXCJoaWRkZW5cIik7XHJcblxyXG5cdHZhciBwYXJ0cyA9IHtcclxuXHRcdGhvdXI6IHsgbmFtZTogJ2hvdXInLCBsaW1pdDogY29uZmlnLmhvdXJzLCAgIHN0YXJ0OiAwLCBlbmQ6IDIsIGN1cnJlbnQ6ICgpID0+IG5ldyBEYXRlKCkuZ2V0SG91cnMoKSB9LFxyXG5cdFx0bWluczogeyBuYW1lOiAnbWlucycsIGxpbWl0OiBjb25maWcubWludXRlcywgc3RhcnQ6IDMsIGVuZDogNSwgY3VycmVudDogKCkgPT4gbmV3IERhdGUoKS5nZXRNaW51dGVzKCkgfSxcclxuXHR9O1xyXG5cclxuXHR2YXIgbWFpbkNvbnRhaW5lciA9IGguY3JlYXRlRWxlbWVudCgnc3BhbicsIHtcclxuXHRcdGNsYXNzOiBbJ2Nocm9tZWQtdGltZXBpY2tlcicsIGAke2NvbmZpZy50aGVtZX0tdGhlbWVgXVxyXG5cdH0pO1xyXG5cclxuXHR2YXIgdGltZVBpY2tlcklucHV0ID0gaC5jcmVhdGVFbGVtZW50KCdpbnB1dCcsIHtcclxuXHRcdGNsYXNzOiBbJ3RpbWUtaW5wdXQnXSxcclxuXHRcdGF0dHI6ICB7dHlwZTogJ3RleHQnfSxcclxuXHRcdHZhbHVlOiB2YWx1ZUlucHV0LnZhbHVlLFxyXG5cdH0pO1xyXG5cclxuXHR2YXIgaW5wdXRDb250YWluZXIgPSBoLmNyZWF0ZUVsZW1lbnQoJ3NwYW4nLCB7XHJcblx0XHRjbGFzczogWydjb250ZW5lZG9yLWlucHV0JywgYGN1cnNvci1zdHlsZS0ke2NvbmZpZy5jdXJzb3JTdHlsZX1gXVxyXG5cdH0pO1xyXG5cclxuXHRoLmluc2VydEFmdGVyKG1haW5Db250YWluZXIsIHZhbHVlSW5wdXQpO1xyXG5cdG1haW5Db250YWluZXIuYXBwZW5kQ2hpbGQoaW5wdXRDb250YWluZXIpO1xyXG5cdGlucHV0Q29udGFpbmVyLmFwcGVuZENoaWxkKHRpbWVQaWNrZXJJbnB1dCk7XHJcblx0bWFpbkNvbnRhaW5lci5hcHBlbmRDaGlsZCh2YWx1ZUlucHV0KTtcclxuXHJcblx0dmFyIHQgPSB0aW1lUGlja2VySW5wdXQ7XHJcblxyXG5cdHZhciBjbG9jayA9IGNyZWF0ZUNsb2NrQnRuRWxlbWVudCgpO1xyXG5cdGlucHV0Q29udGFpbmVyLmFwcGVuZENoaWxkKGNsb2NrKTtcclxuXHJcblx0ZnVuY3Rpb24gZ2V0T3B0aW9uc0NvbnRhaW5lcnMoKSB7XHJcblx0XHRyZXR1cm4gQXJyYXkuZnJvbShtYWluQ29udGFpbmVyLmNoaWxkcmVuKVxyXG5cdFx0XHQuZmlsdGVyKGVsZW0gPT4gZWxlbS5jbGFzc0xpc3QuY29udGFpbnMoJ29wdGlvbnMtY29udGFpbmVycycpKS5zaGlmdCgpO1xyXG5cdH1cclxuXHJcblx0ZnVuY3Rpb24gZHJvcGRvd25TaG93bigpIHtcclxuXHRcdHJldHVybiAhIWdldE9wdGlvbnNDb250YWluZXJzKCk7XHJcblx0fVxyXG5cclxuXHRmdW5jdGlvbiByZW1vdmVEcm9wZG93bigpIHtcclxuXHRcdGNvbnN0IG9wdGlvbnNDb250YWluZXJzID0gZ2V0T3B0aW9uc0NvbnRhaW5lcnMoKTtcclxuXHRcdG9wdGlvbnNDb250YWluZXJzICYmIG9wdGlvbnNDb250YWluZXJzLnJlbW92ZSgpO1xyXG5cdH1cclxuXHJcblx0ZnVuY3Rpb24gc2hvd1VwKCkge1xyXG5cdFx0Y29uc3QgZWxlbVRvcCA9IGguZ2V0T2Zmc2V0VG9wKHQpO1xyXG5cdFx0Y29uc3Qgd25kd1RvcCA9IHdpbmRvdy5zY3JvbGxZO1xyXG5cdFx0Y29uc3QgdG9wRGlmZiA9IGVsZW1Ub3AgLSB3bmR3VG9wO1xyXG5cdFx0Y29uc3QgZWxlbUJvdHRvbSA9IGguZ2V0T2Zmc2V0VG9wKHQpICsgdC5vZmZzZXRIZWlnaHQ7XHJcblx0XHRjb25zdCB3bmR3Qm90dG9tID0gd2luZG93LnNjcm9sbFkgKyB3aW5kb3cuaW5uZXJIZWlnaHQ7XHJcblx0XHRjb25zdCBib3R0b21EaWZmID0gd25kd0JvdHRvbSAtIGVsZW1Cb3R0b207XHJcblx0XHRjb25zdCBlc3RpbWF0ZWRIZWlnaHQgPSAyNjI7IC8vIEhDIC8vIFRPRE86IE1lZGlyIGRlIGFsZ3VuYSBmb3JtYSEhIVxyXG5cclxuXHRcdHJldHVybiBlbGVtVG9wID4gd25kd1RvcCAvLyBFbCBlbGVtZW50byBlc3TDoSBhbCBtZW5vcyB2aXNpYmxlIG8gYWJham8gZGUgbGEgcGFudGFsbGFcclxuXHRcdFx0JiYgYm90dG9tRGlmZiA8IGVzdGltYXRlZEhlaWdodCAvLyBFbCBkZXNwbGVnYWJsZSBubyBlbnRyYSBhYmFqb1xyXG5cdFx0XHQmJiBib3R0b21EaWZmIDwgdG9wRGlmZjsgLy8gQXJyaWJhIGhheSBtw6FzIGx1Z2FyIHBhcmEgbW9zdHJhclxyXG5cdH1cclxuXHJcblx0dmFyIGluaXRpYWxTdGF0ZSA9IHtcclxuXHRcdGhvdXI6IHt2YWx1ZTogbnVsbCwgb2Zmc2V0OiBudWxsLCBzZWxlY3RlZDogbnVsbH0sXHJcblx0XHRtaW5zOiB7dmFsdWU6IG51bGwsIG9mZnNldDogbnVsbCwgc2VsZWN0ZWQ6IG51bGx9LFxyXG5cdH07XHJcblxyXG5cdC8vIEV2ZW50byBjb21wYXJ0aWRvIGluZGVwZW5kaWVudGUgZGUgcXXDqSBzZWNjacOzbiBkZSBvcGNpb25lcyBlc3TDqSBhY3RpdmFcclxuXHRodG1sLmFkZEV2ZW50TGlzdGVuZXIoJ2tleWRvd24nLCAoZXZ0KSA9PiB7XHJcblx0XHRjb25zdCBrZXkgPSBldnQua2V5O1xyXG5cdFx0aWYgKGtleSA9PT0gJ0VzY2FwZScgJiYgZHJvcGRvd25TaG93bigpKSB7XHJcblx0XHRcdC8vIFNpIG5vIGNhbWJpbyBlbCB2YWxvciBkZSBsYSBob3JhIGHDum4sIGNpZXJyYSBsYXMgb3BjaW9uZXNcclxuXHRcdFx0Y29uc3QgY3VycmVudFN0YXRlID0gZ2V0Q3VycmVudFN0YXRlKCk7XHJcblx0XHRcdGlmIChjdXJyZW50U3RhdGUuaG91ci52YWx1ZSA9PT0gaW5pdGlhbFN0YXRlLmhvdXIudmFsdWVcclxuXHRcdFx0ICYmIGN1cnJlbnRTdGF0ZS5taW5zLnZhbHVlID09PSBpbml0aWFsU3RhdGUubWlucy52YWx1ZSkge1xyXG5cdFx0XHRcdHJldHVybiByZW1vdmVEcm9wZG93bigpO1xyXG5cdFx0XHR9XHJcblx0XHRcdC8vIFNpIHlhIGNhbWJpbywgcmVzdGFibGVjZSBlbCB2YWxvciBvcmlnaW5hbCBhbnRlcyBkZWwgY2FtYmlvXHJcblx0XHRcdEFycmF5LmZyb20oZ2V0T3B0aW9uc0NvbnRhaW5lcnMoKS5jaGlsZHJlbikuZm9yRWFjaChvcHRpb25Db250YWluZXIgPT5cclxuXHRcdFx0e1xyXG5cdFx0XHRcdGNvbnN0IHRpbWVQYXJ0ID0gb3B0aW9uQ29udGFpbmVyLmRhdGFzZXQudGltZVBhcnQ7XHJcblx0XHRcdFx0Y29uc3QgaW5pdGlhbFN0YXRlID0gaW5pdGlhbFN0YXRlW3RpbWVQYXJ0XTtcclxuXHJcblx0XHRcdFx0c2V0VGltZVBhcnQodGltZVBhcnQsIGluaXRpYWxTdGF0ZS52YWx1ZSk7XHJcblx0XHRcdFx0Ly8gVE9ETzogRXN0ZSBzY3JvbGwgcG9kcsOtYSBzb2x1Y2lvbmFyc2Ugc2kgbGEgZnVuY2nDs24gbWFya1ZhbHVlIGFjZXB0YXJhIHVuXHJcblx0XHRcdFx0Ly8gICAgICAgcGFyw6FtZXRybyBwYXJhIGZvcnphciBlbCBtb3N0cmFybG8gYXJyaWJhXHJcblx0XHRcdFx0cGFydHNbdGltZVBhcnRdLmxpc3RDb250YWluZXIuc2Nyb2xsVG9wID0gaW5pdGlhbFN0YXRlLm9mZnNldDsgXHJcblx0XHRcdFx0cGFydHNbdGltZVBhcnRdLm1hcmtWYWx1ZShpbml0aWFsU3RhdGUuc2VsZWN0ZWQpO1xyXG5cdFx0XHR9KTtcclxuXHRcdH1cclxuXHR9KTtcclxuXHJcblx0ZnVuY3Rpb24gZ2V0Q3VycmVudFN0YXRlKCkge1xyXG5cdFx0dmFyIHJlc3VsdCA9IHt9O1xyXG5cdFx0QXJyYXkuZnJvbShnZXRPcHRpb25zQ29udGFpbmVycygpLmNoaWxkcmVuKS5mb3JFYWNoKG9wdGlvbkNvbnRhaW5lciA9PiB7XHJcblx0XHRcdGNvbnN0IHRpbWVQYXJ0ID0gb3B0aW9uQ29udGFpbmVyLmRhdGFzZXQudGltZVBhcnQ7XHJcblx0XHRcdHJlc3VsdFt0aW1lUGFydF0gPSB7XHJcblx0XHRcdFx0dmFsdWU6IGdldFRpbWVQYXJ0KHRpbWVQYXJ0KSxcclxuXHRcdFx0XHRvZmZzZXQ6IHBhcnRzW3RpbWVQYXJ0XS5saXN0Q29udGFpbmVyLnNjcm9sbFRvcCxcclxuXHRcdFx0XHRzZWxlY3RlZDogZ2V0VGltZVBhcnRPckN1cnJlbnQodGltZVBhcnQpXHJcblx0XHRcdH07XHJcblx0XHR9KTtcclxuXHRcdHJldHVybiByZXN1bHQ7XHJcblx0fVxyXG5cclxuXHRmdW5jdGlvbiBzaG93RHJvcGRvd24oKVxyXG5cdHtcclxuXHRcdGZ1bmN0aW9uIG51bWJlckVsZW0odmFsLCBjbGFzZSkge1xyXG5cdFx0XHRjb25zdCBzdHJOdW0gPSBudW1Ub1N0cih2YWwpO1xyXG5cdFx0XHR2YXIgbGkgPSBoLmNyZWF0ZUVsZW1lbnQoJ2xpJywge1xyXG5cdFx0XHRcdGNsYXNzOiBbJ251bWVybycsIGNsYXNlXSxcclxuXHRcdFx0XHRkYXRhOiB7IHZhbHVlOiBzdHJOdW0gfVxyXG5cdFx0XHR9KTtcclxuXHRcdFx0bGkuaW5uZXJUZXh0ID0gc3RyTnVtO1xyXG5cdFx0XHRyZXR1cm4gbGk7XHJcblx0XHR9XHJcblx0XHRmdW5jdGlvbiBwYXJ0aWFsTGlzdChuLCBjbGFzZSkge1xyXG5cdFx0XHR2YXIgcmVzdWx0ID0gW107XHJcblx0XHRcdGZvciAodmFyIGkgPSAwOyBpIDwgbjsgaSsrKSB7XHJcblx0XHRcdFx0cmVzdWx0LnB1c2gobnVtYmVyRWxlbShpLCBjbGFzZSkpO1xyXG5cdFx0XHR9XHJcblx0XHRcdHJldHVybiByZXN1bHQ7XHJcblx0XHR9XHJcblx0XHRmdW5jdGlvbiBjb21wbGV0ZUxpc3Qobikge1xyXG5cdFx0XHRyZXR1cm4gWydwcmUnLCAnb3JpZ2luYWwnLCAncG9zdCddXHJcblx0XHRcdFx0Lm1hcChjbGFzZSA9PiBwYXJ0aWFsTGlzdChuLCBjbGFzZSkpXHJcblx0XHRcdFx0LmZsYXQoKTtcclxuXHRcdH1cclxuXHJcblx0XHR2YXIgb3B0aW9uc0NvbnRhaW5lcnMgPSBoLmNyZWF0ZUVsZW1lbnQoJ2RpdicsIHtcclxuXHRcdFx0Y2xhc3M6IFtcclxuXHRcdFx0XHQnb3B0aW9ucy1jb250YWluZXJzJywgXHJcblx0XHRcdFx0c2hvd1VwKCkgPyAnc2hvdy1vbi10b3AnIDogbnVsbCxcclxuXHRcdFx0XSxcclxuXHRcdH0pO1xyXG5cclxuXHRcdGguaW5zZXJ0QWZ0ZXIob3B0aW9uc0NvbnRhaW5lcnMsIGlucHV0Q29udGFpbmVyKTtcclxuXHJcblx0XHRPYmplY3QudmFsdWVzKHBhcnRzKS5mb3JFYWNoKG9wdGlvbiA9PiB7XHJcblx0XHRcdHZhciBjb250YWluZXIgPSBoLmNyZWF0ZUVsZW1lbnQoJ2RpdicsIHtcclxuXHRcdFx0XHRjbGFzczogW1xyXG5cdFx0XHRcdFx0XCJvcHRpb25zLWNvbnRhaW5lclwiLFxyXG5cdFx0XHRcdFx0YCR7b3B0aW9uLm5hbWV9LWNvbnRhaW5lcmBcclxuXHRcdFx0XHRdLFxyXG5cdFx0XHRcdGRhdGE6IHtcclxuXHRcdFx0XHRcdGxpbWl0OiBvcHRpb24ubGltaXQsXHJcblx0XHRcdFx0XHR0aW1lUGFydDogb3B0aW9uLm5hbWUsXHJcblx0XHRcdFx0fVxyXG5cdFx0XHR9KTtcclxuXHRcdFx0b3B0aW9uc0NvbnRhaW5lcnMuYXBwZW5kQ2hpbGQoY29udGFpbmVyKTtcclxuXHRcdH0pO1xyXG5cclxuXHRcdHZhciBhY3RpdmVEcm9wZG93blBhcnQgPSBudWxsO1xyXG5cclxuXHRcdGZ1bmN0aW9uIHNldFZhbHVlRnJvbURyb3Bkb3duKCkge1xyXG5cdFx0XHRPYmplY3QudmFsdWVzKHBhcnRzKS5mb3JFYWNoKHBhcnQgPT4gcGFydC5zZXRQYXJ0RnJvbURyb3Bkb3duKCkpO1xyXG5cdFx0fVxyXG5cclxuXHRcdGZ1bmN0aW9uIGFjdGl2YXRlRHJvcGRvd24ocGFydE5hbWUpIHtcclxuXHRcdFx0YWN0aXZlRHJvcGRvd25QYXJ0ID0gcGFydE5hbWU7XHJcblx0XHRcdEFycmF5LmZyb20ob3B0aW9uc0NvbnRhaW5lcnMuY2hpbGRyZW4pLmZvckVhY2goY29udCA9PiB7XHJcblx0XHRcdFx0aWYgKGNvbnQuY2xhc3NMaXN0LmNvbnRhaW5zKGAke3BhcnROYW1lfS1jb250YWluZXJgKSkge1xyXG5cdFx0XHRcdFx0Y29udC5jbGFzc0xpc3QuYWRkKCdhY3RpdmUtY29udGFpbmVyJyk7XHJcblx0XHRcdFx0fSBlbHNlIHtcclxuXHRcdFx0XHRcdGNvbnQuY2xhc3NMaXN0LnJlbW92ZSgnYWN0aXZlLWNvbnRhaW5lcicpO1xyXG5cdFx0XHRcdH1cclxuXHRcdFx0fSk7XHJcblx0XHR9XHJcblxyXG5cdFx0QXJyYXkuZnJvbShvcHRpb25zQ29udGFpbmVycy5jaGlsZHJlbikuZm9yRWFjaChvcHRpb25Db250YWluZXIgPT5cclxuXHRcdHtcclxuXHRcdFx0dmFyIGxpc3RDb250YWluZXIgPSBoLmNyZWF0ZUVsZW1lbnQoJ2RpdicpO1xyXG5cclxuXHRcdFx0Y29uc3QgdGltZVBhcnQgPSBvcHRpb25Db250YWluZXIuZGF0YXNldC50aW1lUGFydDtcclxuXHRcdFx0Y29uc3QgbGltaXQgPSBwYXJzZUludChvcHRpb25Db250YWluZXIuZGF0YXNldC5saW1pdCk7XHJcblxyXG5cdFx0XHRwYXJ0c1t0aW1lUGFydF0uc2V0UGFydEZyb21Ecm9wZG93biA9ICgpID0+IHtcclxuXHRcdFx0XHRjb25zdCBzZWxlY3RlZCA9IEFycmF5LmZyb20obGlzdENvbnRhaW5lci5jaGlsZHJlblswXS5jaGlsZHJlbilcclxuXHRcdFx0XHRcdC5maWx0ZXIoZWxlbSA9PiBlbGVtLmNsYXNzTGlzdC5jb250YWlucygnc2VsZWN0ZWQnKSk7XHJcblx0XHRcdFx0aWYgKHNlbGVjdGVkLmxlbmd0aCkge1xyXG5cdFx0XHRcdFx0c2V0VGltZVBhcnQodGltZVBhcnQsIHNlbGVjdGVkWzBdLmRhdGFzZXQudmFsdWUpO1xyXG5cdFx0XHRcdH1cclxuXHRcdFx0fTtcclxuXHJcblx0XHRcdHBhcnRzW3RpbWVQYXJ0XS5saXN0Q29udGFpbmVyID0gbGlzdENvbnRhaW5lcjtcclxuXHJcblx0XHRcdGxpc3RDb250YWluZXIuY2xhc3NMaXN0LmFkZChcImxpc3QtY29udGFpbmVyXCIpO1xyXG5cdFx0XHR2YXIgdWwgPSBoLmNyZWF0ZUVsZW1lbnQoJ3VsJyk7XHJcblx0XHRcdGNvbXBsZXRlTGlzdChvcHRpb25Db250YWluZXIuZGF0YXNldC5saW1pdCkuZm9yRWFjaChsaSA9PiB7XHJcblx0XHRcdFx0dWwuYXBwZW5kQ2hpbGQobGkpO1xyXG5cdFx0XHR9KTtcclxuXHRcdFx0bGlzdENvbnRhaW5lci5hcHBlbmRDaGlsZCh1bCk7XHJcblx0XHRcdG9wdGlvbkNvbnRhaW5lci5hcHBlbmRDaGlsZChsaXN0Q29udGFpbmVyKTtcclxuXHJcblx0XHRcdGNvbnN0IHZhbHVlcyA9IEFycmF5LmZyb20obGlzdENvbnRhaW5lci5jaGlsZHJlblswXS5jaGlsZHJlbilcclxuXHRcdFx0XHQuZmlsdGVyKGVsZW0gPT4gZWxlbS5jbGFzc0xpc3QuY29udGFpbnMoJ251bWVybycpKTtcclxuXHRcdFx0Y29uc3QgbiA9IG9wdGlvbkNvbnRhaW5lci5kYXRhc2V0LmxpbWl0OyAvLyBUT0RPOiBGYWx0YSBhcGxpY2FyIGxvIGRlbCBzdGVwXHJcblx0XHRcdGNvbnN0IGhndCA9IHZhbHVlc1swXS5vZmZzZXRIZWlnaHQ7XHJcblx0XHRcdGNvbnN0IGhndFQgPSBoZ3QgKiBuO1xyXG5cclxuXHRcdFx0b3B0aW9uQ29udGFpbmVyLnN0eWxlLmhlaWdodCA9IChoZ3QgKiBjb25maWcub3B0aW9uc0Ftb3VudCArIDMpICsgJ3B4JzsgLy8gSEMgc2Vnw7puIG1hcmdlblxyXG5cdFx0XHRsaXN0Q29udGFpbmVyLnN0eWxlLmhlaWdodCAgID0gKGhndCAqIGNvbmZpZy5vcHRpb25zQW1vdW50ICsgMikgKyAncHgnOyAvLyBIQyBzZWfDum4gbWFyZ2VuXHJcblxyXG5cdFx0XHRsaXN0Q29udGFpbmVyLnNjcm9sbFRvcCA9IGhndFQ7XHJcblxyXG5cdFx0XHRmdW5jdGlvbiBhZGp1c3RQYXJ0aWFsTGlzdCgpIHtcclxuXHRcdFx0XHRpZiAobGlzdENvbnRhaW5lci5zY3JvbGxUb3AgKyBsaXN0Q29udGFpbmVyLm9mZnNldEhlaWdodCA+IGhndFQgKiAzIC0gaGd0KSB7XHJcblx0XHRcdFx0XHRsaXN0Q29udGFpbmVyLnNjcm9sbFRvcCAtPSBoZ3RUO1xyXG5cdFx0XHRcdH1cclxuXHRcdFx0XHRpZiAobGlzdENvbnRhaW5lci5zY3JvbGxUb3AgPCBoZ3QpIHtcclxuXHRcdFx0XHRcdGxpc3RDb250YWluZXIuc2Nyb2xsVG9wICs9IGhndFQ7XHJcblx0XHRcdFx0fVxyXG5cdFx0XHR9XHJcblxyXG5cdFx0XHRmdW5jdGlvbiBjb3JyZWN0QWxpZ25tZW50KCkge1xyXG5cdFx0XHRcdGxpc3RDb250YWluZXIuc2Nyb2xsVG9wID0gTWF0aC5yb3VuZChsaXN0Q29udGFpbmVyLnNjcm9sbFRvcCAvIGhndCkgKiBoZ3Q7XHJcblx0XHRcdH1cclxuXHJcblx0XHRcdGZ1bmN0aW9uIG1hcmtWYWx1ZU9uVG9wKHZhbHVlKSB7XHJcblx0XHRcdFx0bGlzdENvbnRhaW5lci5zY3JvbGxUb3AgPSBoZ3QgKiB2YWx1ZTtcclxuXHRcdFx0XHRtYXJrVmFsdWUodmFsdWUpO1xyXG5cdFx0XHR9XHJcblxyXG5cdFx0XHRmdW5jdGlvbiBtYXJrVmFsdWUodmFsdWUpIFxyXG5cdFx0XHR7XHJcblx0XHRcdFx0dmFsdWVzLmZvckVhY2goZWxlbVNlbCA9PiB7XHJcblx0XHRcdFx0XHRjb25zdCB2YWxDb21wID0gZWxlbVNlbC5kYXRhc2V0LnZhbHVlO1xyXG5cdFx0XHRcdFx0ZWxlbVNlbC5jbGFzc0xpc3RbdmFsQ29tcCA9PT0gdmFsdWUgPyAnYWRkJyA6ICdyZW1vdmUnXSgnc2VsZWN0ZWQnKTtcclxuXHRcdFx0XHR9KTtcclxuXHJcblx0XHRcdFx0Y29uc3Qgc2VsZWN0ZWRFbGVtcyA9IEFycmF5LmZyb20obGlzdENvbnRhaW5lci5jaGlsZHJlblswXS5jaGlsZHJlbilcclxuXHRcdFx0XHRcdC5maWx0ZXIoZWxlbSA9PiBlbGVtLmNsYXNzTGlzdC5jb250YWlucygnc2VsZWN0ZWQnKSk7XHJcblxyXG5cdFx0XHRcdHZhciBuZWFybmVzcyA9IHt9O1xyXG5cdFx0XHRcdHNlbGVjdGVkRWxlbXMuZm9yRWFjaChlbGVtID0+IHtcclxuXHRcdFx0XHRcdGNvbnN0IGRpc3RhbmNlID0gTWF0aC5taW4oXHJcblx0XHRcdFx0XHRcdE1hdGguYWJzKGVsZW0ub2Zmc2V0VG9wIC0gbGlzdENvbnRhaW5lci5zY3JvbGxUb3ApLFxyXG5cdFx0XHRcdFx0XHRNYXRoLmFicyhlbGVtLm9mZnNldEhlaWdodCArIGVsZW0ub2Zmc2V0VG9wIC0gbGlzdENvbnRhaW5lci5zY3JvbGxUb3AgLSBsaXN0Q29udGFpbmVyLm9mZnNldEhlaWdodClcclxuXHRcdFx0XHRcdCk7XHJcblx0XHRcdFx0XHRuZWFybmVzc1tkaXN0YW5jZV0gPSBlbGVtO1xyXG5cdFx0XHRcdH0pO1xyXG5cclxuXHRcdFx0XHRjb25zdCB0b3BTcGFjZSA9IHBhcnNlSW50KHdpbmRvdy5nZXRDb21wdXRlZFN0eWxlKGxpc3RDb250YWluZXIsIG51bGwpWydwYWRkaW5nLXRvcCddKTtcclxuXHRcdFx0XHRjb25zdCBib3R0b21TcGFjZSA9IHBhcnNlSW50KHdpbmRvdy5nZXRDb21wdXRlZFN0eWxlKGxpc3RDb250YWluZXIsIG51bGwpWydwYWRkaW5nLWJvdHRvbSddKSArIDI7IC8vIEhDOiBTYWxpw7MgZGUgcHJ1ZWJhIHkgZXJyb3JcclxuXHRcdFx0XHRcclxuXHRcdFx0XHQvLyBFdml0YXIgcXVlIHNlIGRlc2JvcmRlIGRlIGxhIHpvbmEgdmlzaWJsZVxyXG5cdFx0XHRcdGNvbnN0IHNlbGVjdGVkID0gbmVhcm5lc3NbTWF0aC5taW4oLi4uT2JqZWN0LmtleXMobmVhcm5lc3MpKV07XHJcblx0XHRcdFx0aWYgKHNlbGVjdGVkKSB7XHJcblx0XHRcdFx0XHQvLyBTaSBlc3TDoSBjb3JyaWRvIGhhY2lhIGFycmliYVxyXG5cdFx0XHRcdFx0aWYgKHNlbGVjdGVkLm9mZnNldFRvcCA8IGxpc3RDb250YWluZXIuc2Nyb2xsVG9wKSB7XHJcblx0XHRcdFx0XHRcdGxpc3RDb250YWluZXIuc2Nyb2xsVG9wID0gc2VsZWN0ZWQub2Zmc2V0VG9wIC0gdG9wU3BhY2U7XHJcblx0XHRcdFx0XHR9XHJcblx0XHRcdFx0XHQvLyBTaSBlc3TDoSBjb3JyaWRvIGhhY2lhIGFiYWpvXHJcblx0XHRcdFx0XHRlbHNlIGlmIChzZWxlY3RlZC5vZmZzZXRIZWlnaHQgKyBzZWxlY3RlZC5vZmZzZXRUb3AgPiBsaXN0Q29udGFpbmVyLnNjcm9sbFRvcCArIGxpc3RDb250YWluZXIub2Zmc2V0SGVpZ2h0KSB7XHJcblx0XHRcdFx0XHRcdGxpc3RDb250YWluZXIuc2Nyb2xsVG9wID0gc2VsZWN0ZWQub2Zmc2V0SGVpZ2h0ICsgc2VsZWN0ZWQub2Zmc2V0VG9wIC0gbGlzdENvbnRhaW5lci5vZmZzZXRIZWlnaHQgKyBib3R0b21TcGFjZTtcclxuXHRcdFx0XHRcdH1cclxuXHRcdFx0XHR9XHJcblx0XHRcdH1cclxuXHJcblx0XHRcdHBhcnRzW3RpbWVQYXJ0XS5tYXJrVmFsdWUgPSBtYXJrVmFsdWU7XHJcblxyXG5cdFx0XHR2YXIgY29ycmVjdEFsaWdubWVudFRpbWVvdXQ7XHJcblx0XHRcdGxpc3RDb250YWluZXIuYWRkRXZlbnRMaXN0ZW5lcignc2Nyb2xsJywgKGV2dCkgPT4ge1xyXG5cdFx0XHRcdC8vIERlc2Zhc2plIGRlIHZ1ZWx0YVxyXG5cdFx0XHRcdGFkanVzdFBhcnRpYWxMaXN0KCk7XHJcblx0XHRcdFx0Ly8gRXN0YWJpbGl6YSBhbGluZWFjacOzblxyXG5cdFx0XHRcdGNsZWFyVGltZW91dChjb3JyZWN0QWxpZ25tZW50VGltZW91dCk7XHJcblx0XHRcdFx0Y29ycmVjdEFsaWdubWVudFRpbWVvdXQgPSBzZXRUaW1lb3V0KGNvcnJlY3RBbGlnbm1lbnQsIDUwMCk7XHJcblx0XHRcdH0pO1xyXG5cclxuXHRcdFx0aHRtbC5hZGRFdmVudExpc3RlbmVyKCdrZXlkb3duJywgKGV2dCkgPT4ge1xyXG5cdFx0XHRcdGlmIChbJ0Y1JywgJ0FsdCddLmluY2x1ZGVzKGV2dC5rZXkpKSB7XHJcblx0XHRcdFx0XHRyZXR1cm47XHJcblx0XHRcdFx0fVxyXG5cdFx0XHRcdGNvbnN0IGtleSA9IGV2dC5rZXk7XHJcblx0XHRcdFx0aWYgKGRyb3Bkb3duU2hvd24oKSAmJiBhY3RpdmVEcm9wZG93blBhcnQgPT09IHRpbWVQYXJ0KSB7XHJcblx0XHRcdFx0XHRldnQucHJldmVudERlZmF1bHQoKTtcdFxyXG5cdFx0XHRcdFx0aWYgKGV2dC5rZXkgPT09ICdFbnRlcicpIHtcclxuXHRcdFx0XHRcdFx0cmVtb3ZlRHJvcGRvd24oKTtcclxuXHRcdFx0XHRcdH1cclxuXHRcdFx0XHRcdGlmIChldnQua2V5ID09PSAnQXJyb3dSaWdodCcpIHtcclxuXHRcdFx0XHRcdFx0YWN0aXZhdGVEcm9wZG93bignbWlucycpO1xyXG5cdFx0XHRcdFx0fVxyXG5cdFx0XHRcdFx0aWYgKGV2dC5rZXkgPT09ICdBcnJvd0xlZnQnKSB7XHJcblx0XHRcdFx0XHRcdGFjdGl2YXRlRHJvcGRvd24oJ2hvdXInKTtcclxuXHRcdFx0XHRcdH1cclxuXHRcdFx0XHRcdGlmIChldnQua2V5ID09PSAnVGFiJykge1xyXG5cdFx0XHRcdFx0XHQvLyBPQlM6IEVsIHRpbWVvdXQgZXMgcGFyYSBwcmV2ZW5pciBxdWUgcHJvY2VzZSBsb3MgZXZlbnRvcyBkZSBsYXMgMiBwYXJ0ZXNcclxuXHRcdFx0XHRcdFx0Ly8geSB0ZXJtaW5lIGVuIGVsIG1pc21vIGx1Z2FyIChob3JhID4gbWludXRvID4gaG9yYSlcclxuXHRcdFx0XHRcdFx0Ly8gVE9ETzogUmVzb2x2ZXIgZGUgdW5hIGZvcm1hIG1lam9yXHJcblx0XHRcdFx0XHRcdHNldFRpbWVvdXQoKCkgPT4ge1xyXG5cdFx0XHRcdFx0XHRcdGFjdGl2YXRlRHJvcGRvd24oYWN0aXZlRHJvcGRvd25QYXJ0ID09PSAnaG91cicgPyAnbWlucycgOiAnaG91cicpO1xyXG5cdFx0XHRcdFx0XHR9KTtcclxuXHRcdFx0XHRcdH1cclxuXHJcblx0XHRcdFx0XHRpZiAoWydBcnJvd1VwJywgJ0Fycm93RG93biddLmluY2x1ZGVzKGtleSkpIHtcclxuXHRcdFx0XHRcdFx0Y29uc3Qgc3RlcCA9IGV2dC5rZXkgPT09ICdBcnJvd1VwJyA/IC0xIDogMTtcclxuXHRcdFx0XHRcdFx0Y29uc3QgbmV3U3ViVmFsID0gbnVtVG9TdHIoXHJcblx0XHRcdFx0XHRcdFx0KHBhcnNlSW50KGdldFRpbWVQYXJ0T3JDdXJyZW50KHRpbWVQYXJ0KSkgKyBzdGVwICsgbGltaXQpICUgbGltaXRcclxuXHRcdFx0XHRcdFx0KTtcclxuXHRcdFx0XHRcdFx0bWFya1ZhbHVlKG5ld1N1YlZhbCk7XHJcblx0XHRcdFx0XHRcdC8vIHNldFZhbHVlRnJvbURyb3Bkb3duKCk7XHJcblx0XHRcdFx0XHR9XHJcblx0XHRcdFx0XHRpZiAoWydBcnJvd1VwJywgJ0Fycm93RG93bicsICdFbnRlciddLmluY2x1ZGVzKGtleSkpIHtcclxuXHRcdFx0XHRcdFx0c2V0VmFsdWVGcm9tRHJvcGRvd24oKTtcclxuXHRcdFx0XHRcdH1cclxuXHRcdFx0XHR9XHJcblx0XHRcdH0pO1xyXG5cclxuXHRcdFx0dmFsdWVzLmZvckVhY2goZWxlbSA9PiB7XHJcblxyXG5cdFx0XHRcdGVsZW0uYWRkRXZlbnRMaXN0ZW5lcignbW91c2Vkb3duJywgKGV2dCkgPT4ge1xyXG5cdFx0XHRcdFx0aWYgKGNvbmZpZy5wcmVzZXJ2ZUZvY3VzT25Ecm9wZG93bikge1xyXG5cdFx0XHRcdFx0XHRwcmV2aW91c1RvU2xpZGVyID0gZG9jdW1lbnQuYWN0aXZlRWxlbWVudDtcclxuXHRcdFx0XHRcdH1cclxuXHRcdFx0XHR9KTtcclxuXHJcblx0XHRcdFx0ZWxlbS5hZGRFdmVudExpc3RlbmVyKCdjbGljaycsIChldnQpID0+IHtcclxuXHRcdFx0XHRcdGNvbnN0IHZhbCA9IGV2dC50YXJnZXQuZGF0YXNldC52YWx1ZTtcclxuXHRcdFx0XHRcdGNvbnN0IFtzdGFydCwgZW5kXSA9IFt0LnNlbGVjdGlvblN0YXJ0LCB0LnNlbGVjdGlvbkVuZF07XHJcblxyXG5cdFx0XHRcdFx0c2V0VGltZVBhcnQodGltZVBhcnQsIHZhbCk7XHJcblx0XHRcdFx0XHRhY3RpdmF0ZURyb3Bkb3duKHRpbWVQYXJ0KTtcclxuXHRcdFx0XHRcdG1hcmtWYWx1ZSh2YWwpO1xyXG5cdFx0XHRcdFx0aWYgKGNvbmZpZy5wcmVzZXJ2ZUZvY3VzT25Ecm9wZG93bikge1xyXG5cdFx0XHRcdFx0XHRpZiAocHJldmlvdXNUb1NsaWRlciA9PT0gdCkge1xyXG5cdFx0XHRcdFx0XHRcdHNldFRpbWVvdXQoKCkgPT4ge1xyXG5cdFx0XHRcdFx0XHRcdFx0dC5mb2N1cygpO1xyXG5cdFx0XHRcdFx0XHRcdFx0dC5zZXRTZWxlY3Rpb25SYW5nZShzdGFydCwgZW5kKTtcclxuXHRcdFx0XHRcdFx0XHR9KTtcclxuXHRcdFx0XHRcdFx0fVxyXG5cdFx0XHRcdFx0fVxyXG5cdFx0XHRcdH0pO1xyXG5cdFx0XHR9KTtcclxuXHJcblx0XHRcdG1hcmtWYWx1ZU9uVG9wKGdldFRpbWVQYXJ0T3JDdXJyZW50KHRpbWVQYXJ0KSk7XHJcblx0XHRcdGFjdGl2YXRlRHJvcGRvd24oJ2hvdXInKTtcclxuXHRcdH0pO1xyXG5cclxuXHRcdGluaXRpYWxTdGF0ZSA9IGdldEN1cnJlbnRTdGF0ZSgpO1xyXG5cdH1cclxuXHJcblx0aHRtbC5hZGRFdmVudExpc3RlbmVyKCdjbGljaycsIChldnQpID0+IHtcclxuXHRcdGlmICghcHJldmVudEdsb2JhbENsaWNrICYmIGRyb3Bkb3duU2hvd24oKSkge1xyXG5cdFx0XHRjb25zdCBsaXN0Q29udGFpbmVyQ2xpY2tlZCA9IGguZ2V0RXZlbnRQYXRoKGV2dCkuZmluZChlbGVtID0+IHsgXHJcblx0XHRcdFx0cmV0dXJuIGVsZW0uY2xhc3NMaXN0IFxyXG5cdFx0XHRcdFx0JiYgZWxlbS5jbGFzc0xpc3QuY29udGFpbnMoJ2xpc3QtY29udGFpbmVyJyk7IFxyXG5cdFx0XHR9KTtcclxuXHRcdFx0aWYgKCFsaXN0Q29udGFpbmVyQ2xpY2tlZCkge1xyXG5cdFx0XHRcdHJlbW92ZURyb3Bkb3duKCk7XHJcblx0XHRcdH1cclxuXHRcdH1cclxuXHRcdHByZXZlbnRHbG9iYWxDbGljayA9IGZhbHNlO1xyXG5cdH0pO1xyXG5cclxuXHRpbml0KCk7XHJcblxyXG5cclxuXHQvKlxyXG5cdHwtLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS1cclxuXHR8IENsb2NrXHJcblx0fC0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLVxyXG5cdCovXHJcblxyXG5cdGZ1bmN0aW9uIGNyZWF0ZUNsb2NrQnRuRWxlbWVudCgpIHtcclxuXHRcdHZhciByZXN1bHQgPSBoLmNyZWF0ZUVsZW1lbnQoJ3NwYW4nLCB7IGNsYXNzOiAnY2xvY2stYnRuJ30pO1xyXG5cclxuXHRcdHZhciBzdmcgPSBoLmNyZWF0ZVN2Z0VsZW1lbnQoJ3N2ZycsIHtcclxuXHRcdFx0J3ZpZXdCb3gnOiAnMCAwIDEwMCAxMDAnLFxyXG5cdFx0XHQneG1sbnMnOiAnaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmcnLFxyXG5cdFx0fSk7XHJcblxyXG5cdFx0dmFyIGNpcmNsZSA9IGguY3JlYXRlU3ZnRWxlbWVudCgnY2lyY2xlJywge1xyXG5cdFx0XHQnY3gnOiAnNTAnLCAnY3knOiAnNTAnLCAncic6ICc0NScsXHJcblx0XHRcdCdmaWxsLW9wYWNpdHknOiAnMCcsICdzdHJva2UnOiAnYmxhY2snLCAnc3Ryb2tlLXdpZHRoJzogJzEwJyxcclxuXHRcdH0pO1xyXG5cclxuXHRcdHZhciBsaW5lMSA9IGguY3JlYXRlU3ZnRWxlbWVudCgnbGluZScsIHtcclxuXHRcdFx0J3gxJzogJzUwJywgJ3kxJzogJzU1JywgJ3gyJzogJzUwJywgJ3kyJzogJzI1JyxcclxuXHRcdFx0J3N0cm9rZSc6ICdibGFjaycsICdzdHJva2Utd2lkdGgnOiAnNy41JyxcclxuXHRcdH0pO1xyXG5cclxuXHRcdHZhciBsaW5lMiA9IGguY3JlYXRlU3ZnRWxlbWVudCgnbGluZScsIHtcclxuXHRcdFx0J3gxJzogJzQ4JywgJ3kxJzogJzUyJywgJ3gyJzogJzc0JywgJ3kyJzogJzY3JyxcclxuXHRcdFx0J3N0cm9rZSc6ICdibGFjaycsICdzdHJva2Utd2lkdGgnOiAnNy41JyxcclxuXHRcdH0pO1xyXG5cclxuXHRcdHJlc3VsdC5hcHBlbmRDaGlsZChzdmcpO1xyXG5cdFx0c3ZnLmFwcGVuZENoaWxkKGNpcmNsZSk7XHJcblx0XHRzdmcuYXBwZW5kQ2hpbGQobGluZTEpO1xyXG5cdFx0c3ZnLmFwcGVuZENoaWxkKGxpbmUyKTtcclxuXHJcblx0XHRyZXR1cm4gcmVzdWx0O1xyXG5cdH1cclxuXHJcblx0Ly8gT0JTOiBVYmljYWNpw7NuIHByb3Zpc29yaWEhISFcclxuXHR2YXIgc3RhcnQsIGVuZDtcclxuXHRcclxuXHR2YXIgcHJldmVudEdsb2JhbENsaWNrID0gZmFsc2U7XHJcblxyXG5cdGNsb2NrLmFkZEV2ZW50TGlzdGVuZXIoJ21vdXNlZG93bicsIChldnQpID0+IHtcclxuXHRcdGlmIChjb25maWcucHJlc2VydmVGb2N1c09uRHJvcGRvd24pIHtcclxuXHRcdFx0cHJldmlvdXNUb1NsaWRlciA9IGRvY3VtZW50LmFjdGl2ZUVsZW1lbnQ7XHJcblx0XHRcdFtzdGFydCwgZW5kXSA9IFt0LnNlbGVjdGlvblN0YXJ0LCB0LnNlbGVjdGlvbkVuZF07XHJcblx0XHR9XHJcblx0XHRpZiAoY29uZmlnLmNsb2NrQmVoYXZpb3IgPT09ICdvcmlnaW5hbCcpIHJlbW92ZURyb3Bkb3duKCk7XHJcblx0fSk7XHJcblx0Y2xvY2suYWRkRXZlbnRMaXN0ZW5lcignY2xpY2snLCAoZXZ0KSA9PiB7XHJcblx0XHRpZiAoIWRyb3Bkb3duU2hvd24oKSkge1xyXG5cdFx0XHRpZiAoY29uZmlnLnByZXNlcnZlRm9jdXNPbkRyb3Bkb3duKSB7XHJcblx0XHRcdFx0dC5mb2N1cygpO1xyXG5cdFx0XHRcdGlmIChwcmV2aW91c1RvU2xpZGVyID09PSB0KSB7XHJcblx0XHRcdFx0XHR0LnNldFNlbGVjdGlvblJhbmdlKHN0YXJ0LCBlbmQpO1xyXG5cdFx0XHRcdH1cclxuXHRcdFx0fVxyXG5cdFx0XHRwcmV2ZW50R2xvYmFsQ2xpY2sgPSB0cnVlO1xyXG5cdFx0XHRzaG93RHJvcGRvd24oKTtcclxuXHRcdH0gZWxzZSB7XHJcblx0XHRcdGlmIChjb25maWcuY2xvY2tCZWhhdmlvciA9PT0gJ29yaWdpbmFsJykgc2hvd0Ryb3Bkb3duKCk7XHJcblx0XHRcdGlmIChjb25maWcuY2xvY2tCZWhhdmlvciA9PT0gJ2FsdGVybmF0ZScpIHJlbW92ZURyb3Bkb3duKCk7XHJcblx0XHR9XHJcblx0fSk7XHJcblxyXG5cclxuXHQvKlxyXG5cdHwtLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS1cclxuXHR8IElucHV0XHJcblx0fC0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLVxyXG5cdCovXHJcblxyXG5cdGZ1bmN0aW9uIGluaXQoKSB7XHJcblx0XHRpZiAoIXQudmFsdWUpIHtcdFxyXG5cdFx0XHR0LnZhbHVlID0gXCItLTotLVwiO1xyXG5cdFx0fVxyXG5cdH1cclxuXHJcblx0ZnVuY3Rpb24gZ2V0VGltZVBhcnQocGFydE5hbWUpIHtcclxuXHRcdHJldHVybiB0LnZhbHVlLnN1YnN0cmluZyhwYXJ0c1twYXJ0TmFtZV0uc3RhcnQsIHBhcnRzW3BhcnROYW1lXS5lbmQpO1xyXG5cdH1cclxuXHRmdW5jdGlvbiBnZXRIb3VyKCkgeyByZXR1cm4gZ2V0VGltZVBhcnQoJ2hvdXInKTsgfVxyXG5cdGZ1bmN0aW9uIGdldE1pbnMoKSB7IHJldHVybiBnZXRUaW1lUGFydCgnbWlucycpOyB9XHJcblxyXG5cdGZ1bmN0aW9uIGdldEN1cnJlbnRWYWwocGFydE5hbWUpIHtcclxuXHRcdHJldHVybiBwYXJ0c1twYXJ0TmFtZV0uY3VycmVudC5jYWxsKG51bGwpO1xyXG5cdH1cclxuXHRmdW5jdGlvbiBnZXRDdXJyZW50SG91cigpIHsgcmV0dXJuIGdldEN1cnJlbnRWYWwoJ2hvdXInKTsgfVxyXG5cdGZ1bmN0aW9uIGdldEN1cnJlbnRNaW5zKCkgeyByZXR1cm4gZ2V0Q3VycmVudFZhbCgnbWlucycpOyB9XHJcblxyXG5cdGZ1bmN0aW9uIGdldFRpbWVQYXJ0T3JDdXJyZW50KHBhcnROYW1lKSB7XHJcblx0XHRjb25zdCBpbmNvbXBsZXRlVmFsdWUgPSBPYmplY3Qua2V5cyhwYXJ0cylcclxuXHRcdFx0LmZpbHRlcihwYXJ0TmFtZSA9PiBpc05hTihwYXJzZUludChnZXRUaW1lUGFydChwYXJ0TmFtZSkpKSlcclxuXHRcdFx0Lmxlbmd0aCA+IDA7XHJcblxyXG5cdFx0cmV0dXJuIGluY29tcGxldGVWYWx1ZSBcclxuXHRcdFx0PyBudW1Ub1N0cihnZXRDdXJyZW50VmFsKHBhcnROYW1lKSkgXHJcblx0XHRcdDogZ2V0VGltZVBhcnQocGFydE5hbWUpO1xyXG5cdH1cclxuXHRmdW5jdGlvbiBnZXRIb3VyT3JDdXJyZW50KCkgeyByZXR1cm4gZ2V0VGltZVBhcnRPckN1cnJlbnQoJ2hvdXInKTsgfVxyXG5cdGZ1bmN0aW9uIGdldE1pbnNPckN1cnJlbnQoKSB7IHJldHVybiBnZXRUaW1lUGFydE9yQ3VycmVudCgnbWlucycpOyB9XHJcblxyXG5cdGZ1bmN0aW9uIHNlbGVjdFRpbWVQYXJ0KHBhcnROYW1lKSB7XHJcblx0XHR0LnNldFNlbGVjdGlvblJhbmdlKHBhcnRzW3BhcnROYW1lXS5zdGFydCwgcGFydHNbcGFydE5hbWVdLmVuZCk7XHJcblx0fVxyXG5cdGZ1bmN0aW9uIHNlbGVjdEhvdXIoKSB7IHJldHVybiBzZWxlY3RUaW1lUGFydCgnaG91cicpOyB9XHJcblx0ZnVuY3Rpb24gc2VsZWN0TWlucygpIHsgcmV0dXJuIHNlbGVjdFRpbWVQYXJ0KCdtaW5zJyk7IH1cclxuXHJcblx0ZnVuY3Rpb24gZm9jdXNUaW1lUGFydChwYXJ0TmFtZSkge1xyXG5cdFx0Zmlyc3REaWdpdCA9IHRydWU7XHJcblx0XHRmb2N1c2VkSW4gPSBwYXJ0TmFtZTtcclxuXHRcdHNlbGVjdFRpbWVQYXJ0KHBhcnROYW1lKTtcclxuXHR9XHJcblx0ZnVuY3Rpb24gZm9jdXNIb3VyKCkgeyByZXR1cm4gZm9jdXNUaW1lUGFydCgnaG91cicpOyB9XHJcblx0ZnVuY3Rpb24gZm9jdXNNaW5zKCkgeyByZXR1cm4gZm9jdXNUaW1lUGFydCgnbWlucycpOyB9XHJcblxyXG5cdGZ1bmN0aW9uIG51bVRvU3RyKHZhbCwgbiA9IDIpIHtcclxuXHRcdHJldHVybiBTdHJpbmcodmFsKS5zdWJzdHJpbmcodmFsLmxlbmd0aCAtIG4pLnBhZFN0YXJ0KG4sICcwJyk7XHJcblx0fVxyXG5cclxuXHRmdW5jdGlvbiBzZXRUaW1lUGFydChwYXJ0TmFtZSwgdmFsKSB7XHJcblx0XHRjb25zdCBbc3RhcnQsIGVuZF0gPSBbcGFydHNbcGFydE5hbWVdLnN0YXJ0LCBwYXJ0c1twYXJ0TmFtZV0uZW5kXTtcclxuXHRcdHQudmFsdWUgPSB0LnZhbHVlLnN1YnN0cmluZygwLCBzdGFydClcclxuXHRcdFx0XHRcdFx0KyBudW1Ub1N0cih2YWwpXHJcblx0XHRcdFx0XHRcdCsgdC52YWx1ZS5zdWJzdHJpbmcoZW5kKTtcclxuXHRcdHNlbGVjdFRpbWVQYXJ0KHBhcnROYW1lKTtcclxuXHJcblx0XHR2YWx1ZUlucHV0LnZhbHVlID0gZ2V0UmVhbFZhbHVlKHQudmFsdWUpO1xyXG5cdFx0aC50cmlnZ2VyRXZlbnQodmFsdWVJbnB1dCwgJ2NoYW5nZScpO1xyXG5cdH1cclxuXHRmdW5jdGlvbiBzZXRIb3VyKHZhbCkgeyBzZXRUaW1lUGFydCgnaG91cicsIHZhbCk7XHR9XHJcblx0ZnVuY3Rpb24gc2V0TWlucyh2YWwpIHsgc2V0VGltZVBhcnQoJ21pbnMnLCB2YWwpO1x0fVxyXG5cclxuXHRmdW5jdGlvbiBnZXRSZWFsVmFsdWUodmFsdWUpIHtcclxuXHRcdHJldHVybiB2YWx1ZS5pbmNsdWRlcygnLScpID8gbnVsbCA6IHZhbHVlO1xyXG5cdH1cclxuXHJcblx0dmFyIGZpcnN0RGlnaXQgPSBmYWxzZTtcclxuXHR2YXIgZm9jdXNlZEluID0gbnVsbDtcclxuXHR2YXIgcHJldmlvdXNUb0lucHV0ID0gbnVsbDtcclxuXHR2YXIgcHJldmlvdXNUb1NsaWRlciA9IG51bGw7XHJcblxyXG5cdC8vIERlc2hhYmlsaXRhIHBlZ2FkbyB5IGFycmFzdHJlXHJcblx0WydwYXN0ZScsICdkcmFnc3RhcnQnLCAnZHJvcCddLmZvckVhY2goKGV2dE5hbWUpID0+IHtcclxuXHRcdHQuYWRkRXZlbnRMaXN0ZW5lcihldnROYW1lLCAoZXZ0KSA9PiB7IGV2dC5wcmV2ZW50RGVmYXVsdCgpOyB9KTtcclxuXHR9KTtcclxuXHJcblx0dC5hZGRFdmVudExpc3RlbmVyKCdmb2N1cycsIChldnQpID0+IHtcclxuXHRcdGV2dC5wcmV2ZW50RGVmYXVsdCgpO1xyXG5cdFx0Zm9jdXNIb3VyKCk7XHJcblx0fSk7XHJcblx0dC5hZGRFdmVudExpc3RlbmVyKCdmb2N1cycsIGNvbmZpZy5vbkZvY3VzKTtcclxuXHJcblx0dC5hZGRFdmVudExpc3RlbmVyKCdmb2N1c291dCcsIChldnQpID0+IHtcclxuXHRcdC8vIENvcnJpZ2UgZXhjZXNvIGRlIG1pbnV0b3NcclxuXHRcdHZhciBtaW5zID0gZ2V0TWlucygpO1xyXG5cdFx0aWYgKCFpc05hTihtaW5zKSAmJiBtaW5zID49IGNvbmZpZy5taW51dGVzKSB7XHJcblx0XHRcdHNldE1pbnMoY29uZmlnLm1pbnV0ZXMgLSAxKTtcclxuXHRcdH1cclxuXHR9KTtcclxuXHR0LmFkZEV2ZW50TGlzdGVuZXIoJ2ZvY3Vzb3V0JywgY29uZmlnLm9uQmx1cik7XHJcblxyXG5cdHQuYWRkRXZlbnRMaXN0ZW5lcignbW91c2Vkb3duJywgKGV2dCkgPT4ge1xyXG5cdFx0cHJldmlvdXNUb0lucHV0ID0gZG9jdW1lbnQuYWN0aXZlRWxlbWVudDtcclxuXHR9KTtcclxuXHJcblx0dC5hZGRFdmVudExpc3RlbmVyKCdrZXlkb3duJywgKGV2dCkgPT4ge1xyXG5cdFx0Ly8gVGVjbGFzIGNvbiBmdW5jaW9uYW1pZW50byBub3JtYWxcclxuXHRcdGlmIChbJ0Y1JywgJ0FsdCddLmluY2x1ZGVzKGV2dC5rZXkpKSB7IHJldHVybjsgfVxyXG5cclxuXHRcdGNvbnN0IHRpbWVQYXJ0ID0gdC5zZWxlY3Rpb25TdGFydCA9PT0gMCA/ICdob3VyJyA6ICdtaW5zJztcclxuXHJcblx0XHQvLyBDYXNvIHBhcnRpY3VsYXIgZGVsIFRhYiwgZnVuY2lvbmEgbm9ybWFsIG8gbm8gc2Vnw7puIGVsIGNhc29cclxuXHRcdGlmIChldnQua2V5ID09PSAnVGFiJyAmJiAhZXZ0LnNoaWZ0S2V5ICYmIHRpbWVQYXJ0ID09PSAnbWlucycpIHsgcmV0dXJuOyB9XHJcblx0XHRpZiAoZXZ0LmtleSA9PT0gJ1RhYicgJiYgZXZ0LnNoaWZ0S2V5ICYmIHRpbWVQYXJ0ID09PSAnaG91cicpIHsgcmV0dXJuOyB9XHJcblxyXG5cdFx0ZXZ0LnByZXZlbnREZWZhdWx0KCk7IC8vIGNvbnNvbGUubG9nKGV2dC5rZXkpO1xyXG5cclxuXHRcdC8vIENhbmNlbGEgdG9kYSBhY2Npw7NuIHBhcmEgZXN0YXMgdGVjbGFzIHNpIGVzdMOhIGFiaWVydG8gZWwgbWVuw7pcclxuXHRcdGlmIChkcm9wZG93blNob3duKCkpIHtcclxuXHRcdFx0aWYgKFsnQXJyb3dSaWdodCcsICdBcnJvd0xlZnQnLCAnQXJyb3dVcCcsICdBcnJvd0Rvd24nLCAnRW50ZXInXS5pbmNsdWRlcyhldnQua2V5KSkge1xyXG5cdFx0XHRcdHJldHVybjtcclxuXHRcdFx0fVxyXG5cdFx0fVxyXG5cclxuXHRcdGlmIChldnQua2V5ID09PSAnVGFiJyAmJiBldnQuc2hpZnRLZXkgJiYgdGltZVBhcnQgPT09ICdtaW5zJykgeyBcclxuXHRcdFx0cmV0dXJuIGZvY3VzSG91cigpO1xyXG5cdFx0fVxyXG5cdFx0aWYgKGV2dC5rZXkgPT09ICdUYWInICYmICFldnQuc2hpZnRLZXkgJiYgdGltZVBhcnQgPT09ICdob3VyJykgeyBcclxuXHRcdFx0cmV0dXJuIGZvY3VzTWlucygpO1xyXG5cdFx0fVxyXG5cclxuXHRcdGlmIChldnQua2V5ID09PSAnQXJyb3dSaWdodCcpIHtcclxuXHRcdFx0cmV0dXJuIGZvY3VzTWlucygpO1xyXG5cdFx0fVxyXG5cdFx0aWYgKGV2dC5rZXkgPT09ICdBcnJvd0xlZnQnKSB7XHJcblx0XHRcdHJldHVybiBmb2N1c0hvdXIoKTtcclxuXHRcdH1cclxuXHJcblx0XHRjb25zdCB2YWx1ZSA9IHQudmFsdWU7XHJcblx0XHRjb25zdCBvbGRTdWJWYWwgPSBnZXRUaW1lUGFydCh0aW1lUGFydCk7XHJcblx0XHRjb25zdCBsaW1pdCA9IGZvY3VzZWRJbiA9PT0gJ2hvdXInID8gY29uZmlnLmhvdXJzIDogY29uZmlnLm1pbnV0ZXM7XHJcblx0XHRsZXQgbmV3U3ViVmFsID0gJyc7XHJcblxyXG5cdFx0aWYgKFsnQXJyb3dVcCcsICdBcnJvd0Rvd24nXS5pbmNsdWRlcyhldnQua2V5KSkge1xyXG5cdFx0XHRjb25zdCBzdGVwID0gZXZ0LmtleSA9PT0gJ0Fycm93VXAnID8gMSA6IC0xO1xyXG5cclxuXHRcdFx0bmV3U3ViVmFsID0gbnVtVG9TdHIoXHJcblx0XHRcdFx0aXNOYU4ocGFyc2VJbnQob2xkU3ViVmFsKSlcclxuXHRcdFx0XHRcdD8gMCA6IChwYXJzZUludChvbGRTdWJWYWwpICsgc3RlcCArIGxpbWl0KSAlIGxpbWl0XHJcblx0XHRcdCk7XHJcblx0XHR9XHJcblxyXG5cdFx0aWYgKFsnQmFja3NwYWNlJywgJ0RlbGV0ZSddLmluY2x1ZGVzKGV2dC5rZXkpKSB7XHJcblx0XHRcdG5ld1N1YlZhbCA9ICctLSc7XHJcblx0XHRcdGlmIChmb2N1c2VkSW4gPT09ICdob3VyJykge1xyXG5cdFx0XHRcdGZvY3VzSG91cigpO1xyXG5cdFx0XHR9XHJcblx0XHR9XHJcblxyXG5cdFx0aWYgKGV2dC5rZXkubWF0Y2goL15bMC05XSQvKSlcclxuXHRcdHtcclxuXHRcdFx0bmV3U3ViVmFsID0gKGZpcnN0RGlnaXQgPyAnMCcgOiBvbGRTdWJWYWwuc3Vic3RyaW5nKDEpKSArIGV2dC5rZXk7XHJcblxyXG5cdFx0XHRpZiAoZm9jdXNlZEluID09PSAnaG91cicpIHtcclxuXHRcdFx0XHQvLyBFeGNlZGlkbyBlbiBsYSBkZWNlbmFcclxuXHRcdFx0XHRpZiAoZmlyc3REaWdpdCAmJiBwYXJzZUludChuZXdTdWJWYWwpICogMTAgPj0gbGltaXQpIHtcclxuXHRcdFx0XHRcdHNldFRpbWVvdXQoZm9jdXNNaW5zKTtcclxuXHRcdFx0XHR9XHJcblx0XHRcdFx0aWYgKCFmaXJzdERpZ2l0KSB7XHJcblx0XHRcdFx0XHQvLyBFeGNlZGlkbyBlbiBsYSB1bmlkYWRcclxuXHRcdFx0XHRcdGlmIChwYXJzZUludChuZXdTdWJWYWwpID49IGxpbWl0KSB7XHJcblx0XHRcdFx0XHRcdG5ld1N1YlZhbCA9IFN0cmluZyhsaW1pdCAtIDEpO1xyXG5cdFx0XHRcdFx0fVxyXG5cdFx0XHRcdFx0c2V0VGltZW91dChmb2N1c01pbnMpO1xyXG5cdFx0XHRcdH1cclxuXHRcdFx0fVxyXG5cdFx0XHRmaXJzdERpZ2l0ID0gZmFsc2U7XHJcblx0XHR9XHJcblxyXG5cdFx0aWYgKG5ld1N1YlZhbCkge1xyXG5cdFx0XHRzZXRUaW1lUGFydCh0aW1lUGFydCwgbmV3U3ViVmFsKTtcclxuXHRcdH1cclxuXHR9KTtcclxuXHJcblx0dC5hZGRFdmVudExpc3RlbmVyKCdtb3VzZXVwJywgKGV2dCkgPT4ge1xyXG5cdFx0ZXZ0LnByZXZlbnREZWZhdWx0KCk7XHJcblx0XHRpZiAodC5zZWxlY3Rpb25TdGFydCA+PSAzKSB7XHJcblx0XHRcdGZvY3VzTWlucygpO1xyXG5cdFx0fSBlbHNlIHtcclxuXHRcdFx0Zm9jdXNIb3VyKCk7XHJcblx0XHR9XHJcblx0fSk7XHJcblxyXG5cdHZhbHVlSW5wdXQuYWRkRXZlbnRMaXN0ZW5lcignY2hhbmdlJywgY29uZmlnLm9uQ2hhbmdlKTtcclxuXHJcblx0cmV0dXJuIHtcclxuXHRcdGRvbUNvbnRhaW5lcjogbWFpbkNvbnRhaW5lcixcclxuXHRcdGZvY3VzOiAoKSA9PiB7IHRpbWVQaWNrZXJJbnB1dC5mb2N1cygpOyB9XHJcblx0fTtcclxufTtcclxuXHJcbmV4cG9ydCBkZWZhdWx0IGNocm9tZWRUaW1lcGlja2VyO1xyXG4iXSwibmFtZXMiOlsiaW5zZXJ0QWZ0ZXIiLCJuZXdOb2RlIiwicmVmZXJlbmNlTm9kZSIsInBhcmVudE5vZGUiLCJpbnNlcnRCZWZvcmUiLCJuZXh0U2libGluZyIsImdldE9mZnNldFRvcCIsImVsZW1lbnQiLCJvZmZzZXRQYXJlbnQiLCJvZmZzZXRUb3AiLCJ0cmlnZ2VyRXZlbnQiLCJldnROYW1lIiwiZG9jdW1lbnQiLCJldnQiLCJjcmVhdGVFdmVudCIsImluaXRFdmVudCIsImRpc3BhdGNoRXZlbnQiLCJmaXJlRXZlbnQiLCJnZXRFdmVudFBhdGgiLCJldmVudCIsInBhdGgiLCJjb21wb3NlZFBhdGgiLCJnZXRCb2R5RWxlbWVudCIsImdldEVsZW1lbnRzQnlUYWdOYW1lIiwiZ2V0SHRtbEVsZW1lbnQiLCJjcmVhdGVFbGVtZW50IiwidGFnbmFtZSIsInBhcmFtcyIsInJlc3VsdCIsImNsYXNzZXMiLCJBcnJheSIsImlzQXJyYXkiLCJmb3JFYWNoIiwiYyIsImNsYXNzTGlzdCIsImFkZCIsImF0dHJpYnV0ZXMiLCJhdHRyIiwiT2JqZWN0Iiwia2V5cyIsIm5hbWUiLCJ2YWx1ZSIsInNldEF0dHJpYnV0ZSIsImRhdGEiLCJkYXRhc2V0IiwiY3JlYXRlU3ZnRWxlbWVudCIsImNyZWF0ZUVsZW1lbnROUyIsImgiLCJjaHJvbWVkVGltZXBpY2tlciIsInByZWRlZlNldHRpbmdzIiwib3JpZ2luYWwiLCJwcmVzZXJ2ZUZvY3VzT25Ecm9wZG93biIsImNsb2NrQmVoYXZpb3IiLCJjdXJzb3JTdHlsZSIsInJlY29tbWVuZGVkIiwidmFsdWVJbnB1dCIsIm9wdGlvbnMiLCJodG1sIiwiZGVmYXVsdENvbmZpZyIsInRoZW1lIiwib3B0aW9uc0Ftb3VudCIsImhvdXJzIiwibWludXRlcyIsIm9uQ2hhbmdlIiwib25Gb2N1cyIsImNvbmZpZyIsIm9wdGlvbiIsInZhbCIsInBhcnRzIiwiaG91ciIsImxpbWl0Iiwic3RhcnQiLCJlbmQiLCJjdXJyZW50IiwiRGF0ZSIsImdldEhvdXJzIiwibWlucyIsImdldE1pbnV0ZXMiLCJtYWluQ29udGFpbmVyIiwidGltZVBpY2tlcklucHV0IiwidHlwZSIsImlucHV0Q29udGFpbmVyIiwiYXBwZW5kQ2hpbGQiLCJ0IiwiY2xvY2siLCJjcmVhdGVDbG9ja0J0bkVsZW1lbnQiLCJnZXRPcHRpb25zQ29udGFpbmVycyIsImZyb20iLCJjaGlsZHJlbiIsImZpbHRlciIsImVsZW0iLCJjb250YWlucyIsInNoaWZ0IiwiZHJvcGRvd25TaG93biIsInJlbW92ZURyb3Bkb3duIiwib3B0aW9uc0NvbnRhaW5lcnMiLCJyZW1vdmUiLCJzaG93VXAiLCJlbGVtVG9wIiwid25kd1RvcCIsIndpbmRvdyIsInNjcm9sbFkiLCJ0b3BEaWZmIiwiZWxlbUJvdHRvbSIsIm9mZnNldEhlaWdodCIsInduZHdCb3R0b20iLCJpbm5lckhlaWdodCIsImJvdHRvbURpZmYiLCJlc3RpbWF0ZWRIZWlnaHQiLCJpbml0aWFsU3RhdGUiLCJvZmZzZXQiLCJzZWxlY3RlZCIsImFkZEV2ZW50TGlzdGVuZXIiLCJrZXkiLCJjdXJyZW50U3RhdGUiLCJnZXRDdXJyZW50U3RhdGUiLCJvcHRpb25Db250YWluZXIiLCJ0aW1lUGFydCIsInNldFRpbWVQYXJ0IiwibGlzdENvbnRhaW5lciIsInNjcm9sbFRvcCIsIm1hcmtWYWx1ZSIsImdldFRpbWVQYXJ0IiwiZ2V0VGltZVBhcnRPckN1cnJlbnQiLCJzaG93RHJvcGRvd24iLCJudW1iZXJFbGVtIiwiY2xhc2UiLCJzdHJOdW0iLCJudW1Ub1N0ciIsImxpIiwiaW5uZXJUZXh0IiwicGFydGlhbExpc3QiLCJuIiwiaSIsInB1c2giLCJjb21wbGV0ZUxpc3QiLCJtYXAiLCJmbGF0IiwidmFsdWVzIiwiY29udGFpbmVyIiwiYWN0aXZlRHJvcGRvd25QYXJ0Iiwic2V0VmFsdWVGcm9tRHJvcGRvd24iLCJwYXJ0Iiwic2V0UGFydEZyb21Ecm9wZG93biIsImFjdGl2YXRlRHJvcGRvd24iLCJwYXJ0TmFtZSIsImNvbnQiLCJwYXJzZUludCIsImxlbmd0aCIsInVsIiwiaGd0IiwiaGd0VCIsInN0eWxlIiwiaGVpZ2h0IiwiYWRqdXN0UGFydGlhbExpc3QiLCJjb3JyZWN0QWxpZ25tZW50IiwiTWF0aCIsInJvdW5kIiwibWFya1ZhbHVlT25Ub3AiLCJlbGVtU2VsIiwidmFsQ29tcCIsInNlbGVjdGVkRWxlbXMiLCJuZWFybmVzcyIsImRpc3RhbmNlIiwibWluIiwiYWJzIiwidG9wU3BhY2UiLCJnZXRDb21wdXRlZFN0eWxlIiwiYm90dG9tU3BhY2UiLCJjb3JyZWN0QWxpZ25tZW50VGltZW91dCIsImNsZWFyVGltZW91dCIsInNldFRpbWVvdXQiLCJpbmNsdWRlcyIsInByZXZlbnREZWZhdWx0Iiwic3RlcCIsIm5ld1N1YlZhbCIsInByZXZpb3VzVG9TbGlkZXIiLCJhY3RpdmVFbGVtZW50IiwidGFyZ2V0Iiwic2VsZWN0aW9uU3RhcnQiLCJzZWxlY3Rpb25FbmQiLCJmb2N1cyIsInNldFNlbGVjdGlvblJhbmdlIiwicHJldmVudEdsb2JhbENsaWNrIiwibGlzdENvbnRhaW5lckNsaWNrZWQiLCJmaW5kIiwiaW5pdCIsInN2ZyIsImNpcmNsZSIsImxpbmUxIiwibGluZTIiLCJzdWJzdHJpbmciLCJnZXRNaW5zIiwiZ2V0Q3VycmVudFZhbCIsImNhbGwiLCJpbmNvbXBsZXRlVmFsdWUiLCJpc05hTiIsInNlbGVjdFRpbWVQYXJ0IiwiZm9jdXNUaW1lUGFydCIsImZpcnN0RGlnaXQiLCJmb2N1c2VkSW4iLCJmb2N1c0hvdXIiLCJmb2N1c01pbnMiLCJTdHJpbmciLCJwYWRTdGFydCIsImdldFJlYWxWYWx1ZSIsInNldE1pbnMiLCJvbkJsdXIiLCJzaGlmdEtleSIsIm9sZFN1YlZhbCIsIm1hdGNoIiwiZG9tQ29udGFpbmVyIl0sIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztFQUFBO0VBQ0E7RUFDQTtFQUVBLFNBQVNBLFdBQVQsQ0FBcUJDLE9BQXJCLEVBQThCQyxhQUE5QixFQUE2QztJQUM1Q0EsYUFBYSxDQUFDQyxVQUFkLENBQXlCQyxZQUF6QixDQUFzQ0gsT0FBdEMsRUFBK0NDLGFBQWEsQ0FBQ0csV0FBN0Q7RUFDQTs7RUFFRCxTQUFTQyxZQUFULENBQXNCQyxPQUF0QixFQUErQjtJQUM5QixJQUFJLENBQUNBLE9BQUwsRUFBYyxPQUFPLENBQVA7SUFDZCxPQUFPRCxZQUFZLENBQUNDLE9BQU8sQ0FBQ0MsWUFBVCxDQUFaLEdBQXFDRCxPQUFPLENBQUNFLFNBQXBEO0VBQ0E7O0VBRUQsU0FBU0MsWUFBVCxDQUFzQkgsT0FBdEIsRUFBK0JJLE9BQS9CLEVBQXdDO0lBQ3ZDLElBQUksaUJBQWlCQyxRQUFyQixFQUErQjtNQUMzQixJQUFJQyxHQUFHLEdBQUdELFFBQVEsQ0FBQ0UsV0FBVCxDQUFxQixZQUFyQixDQUFWO01BQ0FELEdBQUcsQ0FBQ0UsU0FBSixDQUFjSixPQUFkLEVBQXVCLEtBQXZCLEVBQThCLElBQTlCO01BQ0FKLE9BQU8sQ0FBQ1MsYUFBUixDQUFzQkgsR0FBdEI7S0FISixNQUlPO01BQ0hOLE9BQU8sQ0FBQ1UsU0FBUixDQUFrQixPQUFPTixPQUF6Qjs7RUFFSjs7RUFFRCxTQUFTTyxZQUFULENBQXNCQyxLQUF0QixFQUE2QjtJQUM1QixPQUFPQSxLQUFLLENBQUNDLElBQU4sSUFBZUQsS0FBSyxDQUFDRSxZQUFOLElBQXNCRixLQUFLLENBQUNFLFlBQU4sRUFBNUM7RUFDQTs7RUFFRCxTQUFTQyxjQUFULEdBQTBCO0lBQ3pCLE9BQU9WLFFBQVEsQ0FBQ1csb0JBQVQsQ0FBOEIsTUFBOUIsRUFBc0MsQ0FBdEMsQ0FBUDtFQUNBOztFQUVELFNBQVNDLGNBQVQsR0FBMEI7SUFDekIsT0FBT1osUUFBUSxDQUFDVyxvQkFBVCxDQUE4QixNQUE5QixFQUFzQyxDQUF0QyxDQUFQO0VBQ0E7O0VBRUQsU0FBU0UsYUFBVCxDQUF1QkMsT0FBdkIsRUFDQTtJQUFBLElBRGdDQyxNQUNoQyx1RUFEeUMsRUFDekM7SUFDQyxJQUFJQyxNQUFNLEdBQUdoQixRQUFRLENBQUNhLGFBQVQsQ0FBdUJDLE9BQXZCLENBQWI7O0lBRUEsSUFBSSxXQUFXQyxNQUFmLEVBQXVCO01BQ3RCLElBQUlFLE9BQU8sR0FBR0YsTUFBTSxTQUFOLElBQWdCLEVBQTlCO01BQ0EsSUFBSSxDQUFDRyxLQUFLLENBQUNDLE9BQU4sQ0FBY0YsT0FBZCxDQUFMLEVBQTZCQSxPQUFPLEdBQUcsQ0FBQ0EsT0FBRCxDQUFWO01BQzdCQSxPQUFPLENBQUNHLE9BQVIsQ0FBZ0IsVUFBQUMsQ0FBQyxFQUFJO1FBQUVBLENBQUMsSUFBSUwsTUFBTSxDQUFDTSxTQUFQLENBQWlCQyxHQUFqQixDQUFxQkYsQ0FBckIsQ0FBTDtPQUF2Qjs7O0lBRUQsSUFBSSxVQUFVTixNQUFkLEVBQXNCO01BQ3JCLElBQUlTLFVBQVUsR0FBR1QsTUFBTSxDQUFDVSxJQUFQLElBQWUsRUFBaEM7TUFDQUMsTUFBTSxDQUFDQyxJQUFQLENBQVlILFVBQVosRUFBd0JKLE9BQXhCLENBQWdDLFVBQUFRLElBQUksRUFBSTtRQUN2QyxJQUFNQyxLQUFLLEdBQUdMLFVBQVUsQ0FBQ0ksSUFBRCxDQUF4QjtRQUNBWixNQUFNLENBQUNjLFlBQVAsQ0FBb0JGLElBQXBCLEVBQTBCQyxLQUExQjtPQUZEOzs7SUFLRCxJQUFJLFVBQVVkLE1BQWQsRUFBc0I7TUFDckIsSUFBSWdCLElBQUksR0FBR2hCLE1BQU0sQ0FBQ2dCLElBQVAsSUFBZSxFQUExQjtNQUNBTCxNQUFNLENBQUNDLElBQVAsQ0FBWUksSUFBWixFQUFrQlgsT0FBbEIsQ0FBMEIsVUFBQVEsSUFBSSxFQUFJO1FBQ2pDLElBQU1DLEtBQUssR0FBR0UsSUFBSSxDQUFDSCxJQUFELENBQWxCO1FBQ0FaLE1BQU0sQ0FBQ2dCLE9BQVAsQ0FBZUosSUFBZixJQUF1QkMsS0FBdkI7T0FGRDs7O0lBS0QsSUFBSSxXQUFXZCxNQUFmLEVBQXVCO01BQ3RCQyxNQUFNLENBQUNhLEtBQVAsR0FBZWQsTUFBTSxDQUFDYyxLQUF0Qjs7O0lBR0QsT0FBT2IsTUFBUDtFQUNBOztFQUVELFNBQVNpQixnQkFBVCxDQUEwQm5CLE9BQTFCLEVBQ0E7SUFBQSxJQURtQ1UsVUFDbkMsdUVBRGdELEVBQ2hEO0lBQ0MsSUFBSVIsTUFBTSxHQUFHaEIsUUFBUSxDQUFDa0MsZUFBVCxDQUF5Qiw0QkFBekIsRUFBdURwQixPQUF2RCxDQUFiO0lBRUFZLE1BQU0sQ0FBQ0MsSUFBUCxDQUFZSCxVQUFaLEVBQXdCSixPQUF4QixDQUFnQyxVQUFBUSxJQUFJLEVBQUk7TUFDdkMsSUFBTUMsS0FBSyxHQUFHTCxVQUFVLENBQUNJLElBQUQsQ0FBeEI7TUFDQVosTUFBTSxDQUFDYyxZQUFQLENBQW9CRixJQUFwQixFQUEwQkMsS0FBMUI7S0FGRDtJQUtBLE9BQU9iLE1BQVA7RUFDQTs7RUFFTSxJQUFJbUIsQ0FBQyxHQUFHO0lBQ2QvQyxXQUFXLEVBQVhBLFdBRGM7SUFFZE0sWUFBWSxFQUFaQSxZQUZjO0lBR2RJLFlBQVksRUFBWkEsWUFIYztJQUlkUSxZQUFZLEVBQVpBLFlBSmM7SUFLZEksY0FBYyxFQUFkQSxjQUxjO0lBTWRFLGNBQWMsRUFBZEEsY0FOYztJQU9kQyxhQUFhLEVBQWJBLGFBUGM7SUFRZG9CLGdCQUFnQixFQUFoQkE7RUFSYyxDQUFSOztFQ3pFUCxJQUFJRyxpQkFBaUIsR0FBRztJQUN2QkMsY0FBYyxFQUFFO01BQ2ZDLFFBQVEsRUFBRTtRQUNUQyx1QkFBdUIsRUFBRSxJQURoQjtRQUVUQyxhQUFhLEVBQUUsVUFGTjtRQUdUQyxXQUFXLEVBQUU7T0FKQztNQU1mQyxXQUFXLEVBQUU7UUFDWkgsdUJBQXVCLEVBQUUsS0FEYjtRQUVaQyxhQUFhLEVBQUUsV0FGSDtRQUdaQyxXQUFXLEVBQUU7OztFQVZRLENBQXhCOztFQWVBTCxpQkFBaUIsQ0FBQ0EsaUJBQWxCLEdBQXNDLFVBQVNPLFVBQVQsRUFDdEM7SUFBQSxJQUQyREMsT0FDM0QsdUVBRHFFLEVBQ3JFO0lBQ0MsSUFBSUMsSUFBSSxHQUFHVixDQUFDLENBQUN2QixjQUFGLEVBQVgsQ0FERDs7SUFJQyxJQUFNa0MsYUFBYSxHQUFHOztNQUVyQkMsS0FBSyxFQUFFLFVBRmM7TUFHckJDLGFBQWEsRUFBRSxDQUhNO01BSXJCVCx1QkFBdUIsRUFBRSxLQUpKO01BS3JCQyxhQUFhLEVBQUUsVUFMTTs7TUFNckJTLEtBQUssRUFBRSxFQU5jO01BT3JCQyxPQUFPLEVBQUUsRUFQWTtNQVFyQlQsV0FBVyxFQUFFLGFBUlE7OztNQVVyQlUsUUFBUSxFQUFFLGtCQUFDbEQsR0FBRCxFQUFTLEVBVkU7TUFXckJtRCxPQUFPLEVBQUUsaUJBQUNuRCxHQUFELEVBQVM7S0FYbkI7O0lBY0EsSUFBSW9ELE1BQU0sc0JBQU9QLGFBQVAsQ0FBVjs7SUFDQXBCLE1BQU0sQ0FBQ0MsSUFBUCxDQUFZaUIsT0FBWixFQUFxQnhCLE9BQXJCLENBQTZCLFVBQUFrQyxNQUFNLEVBQUk7TUFDdEMsSUFBTUMsR0FBRyxHQUFHWCxPQUFPLENBQUNVLE1BQUQsQ0FBbkI7O01BQ0EsSUFBSUMsR0FBRyxLQUFLLElBQVosRUFBa0I7UUFDakJGLE1BQU0sQ0FBQ0MsTUFBRCxDQUFOLEdBQWlCQyxHQUFqQjs7S0FIRjtJQU9BWixVQUFVLENBQUNiLFlBQVgsQ0FBd0IsTUFBeEIsRUFBZ0MsUUFBaEM7SUFFQSxJQUFJMEIsS0FBSyxHQUFHO01BQ1hDLElBQUksRUFBRTtRQUFFN0IsSUFBSSxFQUFFLE1BQVI7UUFBZ0I4QixLQUFLLEVBQUVMLE1BQU0sQ0FBQ0osS0FBOUI7UUFBdUNVLEtBQUssRUFBRSxDQUE5QztRQUFpREMsR0FBRyxFQUFFLENBQXREO1FBQXlEQyxPQUFPLEVBQUU7VUFBQSxPQUFNLElBQUlDLElBQUosR0FBV0MsUUFBWCxFQUFOOztPQUQ3RDtNQUVYQyxJQUFJLEVBQUU7UUFBRXBDLElBQUksRUFBRSxNQUFSO1FBQWdCOEIsS0FBSyxFQUFFTCxNQUFNLENBQUNILE9BQTlCO1FBQXVDUyxLQUFLLEVBQUUsQ0FBOUM7UUFBaURDLEdBQUcsRUFBRSxDQUF0RDtRQUF5REMsT0FBTyxFQUFFO1VBQUEsT0FBTSxJQUFJQyxJQUFKLEdBQVdHLFVBQVgsRUFBTjs7O0tBRnpFO0lBS0EsSUFBSUMsYUFBYSxHQUFHL0IsQ0FBQyxDQUFDdEIsYUFBRixDQUFnQixNQUFoQixFQUF3QjtNQUMzQyxTQUFPLENBQUMsb0JBQUQsWUFBMEJ3QyxNQUFNLENBQUNOLEtBQWpDO0tBRFksQ0FBcEI7SUFJQSxJQUFJb0IsZUFBZSxHQUFHaEMsQ0FBQyxDQUFDdEIsYUFBRixDQUFnQixPQUFoQixFQUF5QjtNQUM5QyxTQUFPLENBQUMsWUFBRCxDQUR1QztNQUU5Q1ksSUFBSSxFQUFHO1FBQUMyQyxJQUFJLEVBQUU7T0FGZ0M7TUFHOUN2QyxLQUFLLEVBQUVjLFVBQVUsQ0FBQ2Q7S0FIRyxDQUF0QjtJQU1BLElBQUl3QyxjQUFjLEdBQUdsQyxDQUFDLENBQUN0QixhQUFGLENBQWdCLE1BQWhCLEVBQXdCO01BQzVDLFNBQU8sQ0FBQyxrQkFBRCx5QkFBcUN3QyxNQUFNLENBQUNaLFdBQTVDO0tBRGEsQ0FBckI7SUFJQU4sQ0FBQyxDQUFDL0MsV0FBRixDQUFjOEUsYUFBZCxFQUE2QnZCLFVBQTdCO0lBQ0F1QixhQUFhLENBQUNJLFdBQWQsQ0FBMEJELGNBQTFCO0lBQ0FBLGNBQWMsQ0FBQ0MsV0FBZixDQUEyQkgsZUFBM0I7SUFDQUQsYUFBYSxDQUFDSSxXQUFkLENBQTBCM0IsVUFBMUI7SUFFQSxJQUFJNEIsQ0FBQyxHQUFHSixlQUFSO0lBRUEsSUFBSUssS0FBSyxHQUFHQyxxQkFBcUIsRUFBakM7SUFDQUosY0FBYyxDQUFDQyxXQUFmLENBQTJCRSxLQUEzQjs7SUFFQSxTQUFTRSxvQkFBVCxHQUFnQztNQUMvQixPQUFPeEQsS0FBSyxDQUFDeUQsSUFBTixDQUFXVCxhQUFhLENBQUNVLFFBQXpCLEVBQ0xDLE1BREssQ0FDRSxVQUFBQyxJQUFJO1FBQUEsT0FBSUEsSUFBSSxDQUFDeEQsU0FBTCxDQUFleUQsUUFBZixDQUF3QixvQkFBeEIsQ0FBSjtPQUROLEVBQ3lEQyxLQUR6RCxFQUFQOzs7SUFJRCxTQUFTQyxhQUFULEdBQXlCO01BQ3hCLE9BQU8sQ0FBQyxDQUFDUCxvQkFBb0IsRUFBN0I7OztJQUdELFNBQVNRLGNBQVQsR0FBMEI7TUFDekIsSUFBTUMsaUJBQWlCLEdBQUdULG9CQUFvQixFQUE5QztNQUNBUyxpQkFBaUIsSUFBSUEsaUJBQWlCLENBQUNDLE1BQWxCLEVBQXJCOzs7SUFHRCxTQUFTQyxNQUFULEdBQWtCO01BQ2pCLElBQU1DLE9BQU8sR0FBR25ELENBQUMsQ0FBQ3pDLFlBQUYsQ0FBZTZFLENBQWYsQ0FBaEI7TUFDQSxJQUFNZ0IsT0FBTyxHQUFHQyxNQUFNLENBQUNDLE9BQXZCO01BQ0EsSUFBTUMsT0FBTyxHQUFHSixPQUFPLEdBQUdDLE9BQTFCO01BQ0EsSUFBTUksVUFBVSxHQUFHeEQsQ0FBQyxDQUFDekMsWUFBRixDQUFlNkUsQ0FBZixJQUFvQkEsQ0FBQyxDQUFDcUIsWUFBekM7TUFDQSxJQUFNQyxVQUFVLEdBQUdMLE1BQU0sQ0FBQ0MsT0FBUCxHQUFpQkQsTUFBTSxDQUFDTSxXQUEzQztNQUNBLElBQU1DLFVBQVUsR0FBR0YsVUFBVSxHQUFHRixVQUFoQztNQUNBLElBQU1LLGVBQWUsR0FBRyxHQUF4QixDQVBpQjs7TUFTakIsT0FBT1YsT0FBTyxHQUFHQyxPQUFWO1NBQ0hRLFVBQVUsR0FBR0MsZUFEVjtTQUVIRCxVQUFVLEdBQUdMLE9BRmpCLENBVGlCOzs7SUFjbEIsSUFBSU8sWUFBWSxHQUFHO01BQ2xCeEMsSUFBSSxFQUFFO1FBQUM1QixLQUFLLEVBQUUsSUFBUjtRQUFjcUUsTUFBTSxFQUFFLElBQXRCO1FBQTRCQyxRQUFRLEVBQUU7T0FEMUI7TUFFbEJuQyxJQUFJLEVBQUU7UUFBQ25DLEtBQUssRUFBRSxJQUFSO1FBQWNxRSxNQUFNLEVBQUUsSUFBdEI7UUFBNEJDLFFBQVEsRUFBRTs7S0FGN0MsQ0FyRkQ7O0lBMkZDdEQsSUFBSSxDQUFDdUQsZ0JBQUwsQ0FBc0IsU0FBdEIsRUFBaUMsVUFBQ25HLEdBQUQsRUFBUztNQUN6QyxJQUFNb0csR0FBRyxHQUFHcEcsR0FBRyxDQUFDb0csR0FBaEI7O01BQ0EsSUFBSUEsR0FBRyxLQUFLLFFBQVIsSUFBb0JwQixhQUFhLEVBQXJDLEVBQXlDOztRQUV4QyxJQUFNcUIsWUFBWSxHQUFHQyxlQUFlLEVBQXBDOztRQUNBLElBQUlELFlBQVksQ0FBQzdDLElBQWIsQ0FBa0I1QixLQUFsQixLQUE0Qm9FLFlBQVksQ0FBQ3hDLElBQWIsQ0FBa0I1QixLQUE5QyxJQUNBeUUsWUFBWSxDQUFDdEMsSUFBYixDQUFrQm5DLEtBQWxCLEtBQTRCb0UsWUFBWSxDQUFDakMsSUFBYixDQUFrQm5DLEtBRGxELEVBQ3lEO1VBQ3hELE9BQU9xRCxjQUFjLEVBQXJCO1NBTHVDOzs7UUFReENoRSxLQUFLLENBQUN5RCxJQUFOLENBQVdELG9CQUFvQixHQUFHRSxRQUFsQyxFQUE0Q3hELE9BQTVDLENBQW9ELFVBQUFvRixlQUFlLEVBQ25FO1VBQ0MsSUFBTUMsUUFBUSxHQUFHRCxlQUFlLENBQUN4RSxPQUFoQixDQUF3QnlFLFFBQXpDO1VBQ0EsSUFBTVIsWUFBWSxHQUFHQSxZQUFZLENBQUNRLFFBQUQsQ0FBakM7VUFFQUMsV0FBVyxDQUFDRCxRQUFELEVBQVdSLFlBQVksQ0FBQ3BFLEtBQXhCLENBQVgsQ0FKRDs7O1VBT0MyQixLQUFLLENBQUNpRCxRQUFELENBQUwsQ0FBZ0JFLGFBQWhCLENBQThCQyxTQUE5QixHQUEwQ1gsWUFBWSxDQUFDQyxNQUF2RDtVQUNBMUMsS0FBSyxDQUFDaUQsUUFBRCxDQUFMLENBQWdCSSxTQUFoQixDQUEwQlosWUFBWSxDQUFDRSxRQUF2QztTQVREOztLQVZGOztJQXdCQSxTQUFTSSxlQUFULEdBQTJCO01BQzFCLElBQUl2RixNQUFNLEdBQUcsRUFBYjtNQUNBRSxLQUFLLENBQUN5RCxJQUFOLENBQVdELG9CQUFvQixHQUFHRSxRQUFsQyxFQUE0Q3hELE9BQTVDLENBQW9ELFVBQUFvRixlQUFlLEVBQUk7UUFDdEUsSUFBTUMsUUFBUSxHQUFHRCxlQUFlLENBQUN4RSxPQUFoQixDQUF3QnlFLFFBQXpDO1FBQ0F6RixNQUFNLENBQUN5RixRQUFELENBQU4sR0FBbUI7VUFDbEI1RSxLQUFLLEVBQUVpRixXQUFXLENBQUNMLFFBQUQsQ0FEQTtVQUVsQlAsTUFBTSxFQUFFMUMsS0FBSyxDQUFDaUQsUUFBRCxDQUFMLENBQWdCRSxhQUFoQixDQUE4QkMsU0FGcEI7VUFHbEJULFFBQVEsRUFBRVksb0JBQW9CLENBQUNOLFFBQUQ7U0FIL0I7T0FGRDtNQVFBLE9BQU96RixNQUFQOzs7SUFHRCxTQUFTZ0csWUFBVCxHQUNBO01BQ0MsU0FBU0MsVUFBVCxDQUFvQjFELEdBQXBCLEVBQXlCMkQsS0FBekIsRUFBZ0M7UUFDL0IsSUFBTUMsTUFBTSxHQUFHQyxRQUFRLENBQUM3RCxHQUFELENBQXZCO1FBQ0EsSUFBSThELEVBQUUsR0FBR2xGLENBQUMsQ0FBQ3RCLGFBQUYsQ0FBZ0IsSUFBaEIsRUFBc0I7VUFDOUIsU0FBTyxDQUFDLFFBQUQsRUFBV3FHLEtBQVgsQ0FEdUI7VUFFOUJuRixJQUFJLEVBQUU7WUFBRUYsS0FBSyxFQUFFc0Y7O1NBRlAsQ0FBVDtRQUlBRSxFQUFFLENBQUNDLFNBQUgsR0FBZUgsTUFBZjtRQUNBLE9BQU9FLEVBQVA7OztNQUVELFNBQVNFLFdBQVQsQ0FBcUJDLENBQXJCLEVBQXdCTixLQUF4QixFQUErQjtRQUM5QixJQUFJbEcsTUFBTSxHQUFHLEVBQWI7O1FBQ0EsS0FBSyxJQUFJeUcsQ0FBQyxHQUFHLENBQWIsRUFBZ0JBLENBQUMsR0FBR0QsQ0FBcEIsRUFBdUJDLENBQUMsRUFBeEIsRUFBNEI7VUFDM0J6RyxNQUFNLENBQUMwRyxJQUFQLENBQVlULFVBQVUsQ0FBQ1EsQ0FBRCxFQUFJUCxLQUFKLENBQXRCOzs7UUFFRCxPQUFPbEcsTUFBUDs7O01BRUQsU0FBUzJHLFlBQVQsQ0FBc0JILENBQXRCLEVBQXlCO1FBQ3hCLE9BQU8sQ0FBQyxLQUFELEVBQVEsVUFBUixFQUFvQixNQUFwQixFQUNMSSxHQURLLENBQ0QsVUFBQVYsS0FBSztVQUFBLE9BQUlLLFdBQVcsQ0FBQ0MsQ0FBRCxFQUFJTixLQUFKLENBQWY7U0FESixFQUVMVyxJQUZLLEVBQVA7OztNQUtELElBQUkxQyxpQkFBaUIsR0FBR2hELENBQUMsQ0FBQ3RCLGFBQUYsQ0FBZ0IsS0FBaEIsRUFBdUI7UUFDOUMsU0FBTyxDQUNOLG9CQURNLEVBRU53RSxNQUFNLEtBQUssYUFBTCxHQUFxQixJQUZyQjtPQURnQixDQUF4QjtNQU9BbEQsQ0FBQyxDQUFDL0MsV0FBRixDQUFjK0YsaUJBQWQsRUFBaUNkLGNBQWpDO01BRUEzQyxNQUFNLENBQUNvRyxNQUFQLENBQWN0RSxLQUFkLEVBQXFCcEMsT0FBckIsQ0FBNkIsVUFBQWtDLE1BQU0sRUFBSTtRQUN0QyxJQUFJeUUsU0FBUyxHQUFHNUYsQ0FBQyxDQUFDdEIsYUFBRixDQUFnQixLQUFoQixFQUF1QjtVQUN0QyxTQUFPLENBQ04sbUJBRE0sWUFFSHlDLE1BQU0sQ0FBQzFCLElBRkosZ0JBRCtCO1VBS3RDRyxJQUFJLEVBQUU7WUFDTDJCLEtBQUssRUFBRUosTUFBTSxDQUFDSSxLQURUO1lBRUwrQyxRQUFRLEVBQUVuRCxNQUFNLENBQUMxQjs7U0FQSCxDQUFoQjtRQVVBdUQsaUJBQWlCLENBQUNiLFdBQWxCLENBQThCeUQsU0FBOUI7T0FYRDtNQWNBLElBQUlDLGtCQUFrQixHQUFHLElBQXpCOztNQUVBLFNBQVNDLG9CQUFULEdBQWdDO1FBQy9CdkcsTUFBTSxDQUFDb0csTUFBUCxDQUFjdEUsS0FBZCxFQUFxQnBDLE9BQXJCLENBQTZCLFVBQUE4RyxJQUFJO1VBQUEsT0FBSUEsSUFBSSxDQUFDQyxtQkFBTCxFQUFKO1NBQWpDOzs7TUFHRCxTQUFTQyxnQkFBVCxDQUEwQkMsUUFBMUIsRUFBb0M7UUFDbkNMLGtCQUFrQixHQUFHSyxRQUFyQjtRQUNBbkgsS0FBSyxDQUFDeUQsSUFBTixDQUFXUSxpQkFBaUIsQ0FBQ1AsUUFBN0IsRUFBdUN4RCxPQUF2QyxDQUErQyxVQUFBa0gsSUFBSSxFQUFJO1VBQ3RELElBQUlBLElBQUksQ0FBQ2hILFNBQUwsQ0FBZXlELFFBQWYsV0FBMkJzRCxRQUEzQixnQkFBSixFQUFzRDtZQUNyREMsSUFBSSxDQUFDaEgsU0FBTCxDQUFlQyxHQUFmLENBQW1CLGtCQUFuQjtXQURELE1BRU87WUFDTitHLElBQUksQ0FBQ2hILFNBQUwsQ0FBZThELE1BQWYsQ0FBc0Isa0JBQXRCOztTQUpGOzs7TUFTRGxFLEtBQUssQ0FBQ3lELElBQU4sQ0FBV1EsaUJBQWlCLENBQUNQLFFBQTdCLEVBQXVDeEQsT0FBdkMsQ0FBK0MsVUFBQW9GLGVBQWUsRUFDOUQ7UUFDQyxJQUFJRyxhQUFhLEdBQUd4RSxDQUFDLENBQUN0QixhQUFGLENBQWdCLEtBQWhCLENBQXBCO1FBRUEsSUFBTTRGLFFBQVEsR0FBR0QsZUFBZSxDQUFDeEUsT0FBaEIsQ0FBd0J5RSxRQUF6QztRQUNBLElBQU0vQyxLQUFLLEdBQUc2RSxRQUFRLENBQUMvQixlQUFlLENBQUN4RSxPQUFoQixDQUF3QjBCLEtBQXpCLENBQXRCOztRQUVBRixLQUFLLENBQUNpRCxRQUFELENBQUwsQ0FBZ0IwQixtQkFBaEIsR0FBc0MsWUFBTTtVQUMzQyxJQUFNaEMsUUFBUSxHQUFHakYsS0FBSyxDQUFDeUQsSUFBTixDQUFXZ0MsYUFBYSxDQUFDL0IsUUFBZCxDQUF1QixDQUF2QixFQUEwQkEsUUFBckMsRUFDZkMsTUFEZSxDQUNSLFVBQUFDLElBQUk7WUFBQSxPQUFJQSxJQUFJLENBQUN4RCxTQUFMLENBQWV5RCxRQUFmLENBQXdCLFVBQXhCLENBQUo7V0FESSxDQUFqQjs7VUFFQSxJQUFJb0IsUUFBUSxDQUFDcUMsTUFBYixFQUFxQjtZQUNwQjlCLFdBQVcsQ0FBQ0QsUUFBRCxFQUFXTixRQUFRLENBQUMsQ0FBRCxDQUFSLENBQVluRSxPQUFaLENBQW9CSCxLQUEvQixDQUFYOztTQUpGOztRQVFBMkIsS0FBSyxDQUFDaUQsUUFBRCxDQUFMLENBQWdCRSxhQUFoQixHQUFnQ0EsYUFBaEM7UUFFQUEsYUFBYSxDQUFDckYsU0FBZCxDQUF3QkMsR0FBeEIsQ0FBNEIsZ0JBQTVCO1FBQ0EsSUFBSWtILEVBQUUsR0FBR3RHLENBQUMsQ0FBQ3RCLGFBQUYsQ0FBZ0IsSUFBaEIsQ0FBVDtRQUNBOEcsWUFBWSxDQUFDbkIsZUFBZSxDQUFDeEUsT0FBaEIsQ0FBd0IwQixLQUF6QixDQUFaLENBQTRDdEMsT0FBNUMsQ0FBb0QsVUFBQWlHLEVBQUUsRUFBSTtVQUN6RG9CLEVBQUUsQ0FBQ25FLFdBQUgsQ0FBZStDLEVBQWY7U0FERDtRQUdBVixhQUFhLENBQUNyQyxXQUFkLENBQTBCbUUsRUFBMUI7UUFDQWpDLGVBQWUsQ0FBQ2xDLFdBQWhCLENBQTRCcUMsYUFBNUI7UUFFQSxJQUFNbUIsTUFBTSxHQUFHNUcsS0FBSyxDQUFDeUQsSUFBTixDQUFXZ0MsYUFBYSxDQUFDL0IsUUFBZCxDQUF1QixDQUF2QixFQUEwQkEsUUFBckMsRUFDYkMsTUFEYSxDQUNOLFVBQUFDLElBQUk7VUFBQSxPQUFJQSxJQUFJLENBQUN4RCxTQUFMLENBQWV5RCxRQUFmLENBQXdCLFFBQXhCLENBQUo7U0FERSxDQUFmO1FBRUEsSUFBTXlDLENBQUMsR0FBR2hCLGVBQWUsQ0FBQ3hFLE9BQWhCLENBQXdCMEIsS0FBbEMsQ0ExQkQ7O1FBMkJDLElBQU1nRixHQUFHLEdBQUdaLE1BQU0sQ0FBQyxDQUFELENBQU4sQ0FBVWxDLFlBQXRCO1FBQ0EsSUFBTStDLElBQUksR0FBR0QsR0FBRyxHQUFHbEIsQ0FBbkI7UUFFQWhCLGVBQWUsQ0FBQ29DLEtBQWhCLENBQXNCQyxNQUF0QixHQUFnQ0gsR0FBRyxHQUFHckYsTUFBTSxDQUFDTCxhQUFiLEdBQTZCLENBQTlCLEdBQW1DLElBQWxFLENBOUJEOztRQStCQzJELGFBQWEsQ0FBQ2lDLEtBQWQsQ0FBb0JDLE1BQXBCLEdBQWdDSCxHQUFHLEdBQUdyRixNQUFNLENBQUNMLGFBQWIsR0FBNkIsQ0FBOUIsR0FBbUMsSUFBbEUsQ0EvQkQ7O1FBaUNDMkQsYUFBYSxDQUFDQyxTQUFkLEdBQTBCK0IsSUFBMUI7O1FBRUEsU0FBU0csaUJBQVQsR0FBNkI7VUFDNUIsSUFBSW5DLGFBQWEsQ0FBQ0MsU0FBZCxHQUEwQkQsYUFBYSxDQUFDZixZQUF4QyxHQUF1RCtDLElBQUksR0FBRyxDQUFQLEdBQVdELEdBQXRFLEVBQTJFO1lBQzFFL0IsYUFBYSxDQUFDQyxTQUFkLElBQTJCK0IsSUFBM0I7OztVQUVELElBQUloQyxhQUFhLENBQUNDLFNBQWQsR0FBMEI4QixHQUE5QixFQUFtQztZQUNsQy9CLGFBQWEsQ0FBQ0MsU0FBZCxJQUEyQitCLElBQTNCOzs7O1FBSUYsU0FBU0ksZ0JBQVQsR0FBNEI7VUFDM0JwQyxhQUFhLENBQUNDLFNBQWQsR0FBMEJvQyxJQUFJLENBQUNDLEtBQUwsQ0FBV3RDLGFBQWEsQ0FBQ0MsU0FBZCxHQUEwQjhCLEdBQXJDLElBQTRDQSxHQUF0RTs7O1FBR0QsU0FBU1EsY0FBVCxDQUF3QnJILEtBQXhCLEVBQStCO1VBQzlCOEUsYUFBYSxDQUFDQyxTQUFkLEdBQTBCOEIsR0FBRyxHQUFHN0csS0FBaEM7VUFDQWdGLFNBQVMsQ0FBQ2hGLEtBQUQsQ0FBVDs7O1FBR0QsU0FBU2dGLFNBQVQsQ0FBbUJoRixLQUFuQixFQUNBO1VBQ0NpRyxNQUFNLENBQUMxRyxPQUFQLENBQWUsVUFBQStILE9BQU8sRUFBSTtZQUN6QixJQUFNQyxPQUFPLEdBQUdELE9BQU8sQ0FBQ25ILE9BQVIsQ0FBZ0JILEtBQWhDO1lBQ0FzSCxPQUFPLENBQUM3SCxTQUFSLENBQWtCOEgsT0FBTyxLQUFLdkgsS0FBWixHQUFvQixLQUFwQixHQUE0QixRQUE5QyxFQUF3RCxVQUF4RDtXQUZEO1VBS0EsSUFBTXdILGFBQWEsR0FBR25JLEtBQUssQ0FBQ3lELElBQU4sQ0FBV2dDLGFBQWEsQ0FBQy9CLFFBQWQsQ0FBdUIsQ0FBdkIsRUFBMEJBLFFBQXJDLEVBQ3BCQyxNQURvQixDQUNiLFVBQUFDLElBQUk7WUFBQSxPQUFJQSxJQUFJLENBQUN4RCxTQUFMLENBQWV5RCxRQUFmLENBQXdCLFVBQXhCLENBQUo7V0FEUyxDQUF0QjtVQUdBLElBQUl1RSxRQUFRLEdBQUcsRUFBZjtVQUNBRCxhQUFhLENBQUNqSSxPQUFkLENBQXNCLFVBQUEwRCxJQUFJLEVBQUk7WUFDN0IsSUFBTXlFLFFBQVEsR0FBR1AsSUFBSSxDQUFDUSxHQUFMLENBQ2hCUixJQUFJLENBQUNTLEdBQUwsQ0FBUzNFLElBQUksQ0FBQ2pGLFNBQUwsR0FBaUI4RyxhQUFhLENBQUNDLFNBQXhDLENBRGdCLEVBRWhCb0MsSUFBSSxDQUFDUyxHQUFMLENBQVMzRSxJQUFJLENBQUNjLFlBQUwsR0FBb0JkLElBQUksQ0FBQ2pGLFNBQXpCLEdBQXFDOEcsYUFBYSxDQUFDQyxTQUFuRCxHQUErREQsYUFBYSxDQUFDZixZQUF0RixDQUZnQixDQUFqQjtZQUlBMEQsUUFBUSxDQUFDQyxRQUFELENBQVIsR0FBcUJ6RSxJQUFyQjtXQUxEO1VBUUEsSUFBTTRFLFFBQVEsR0FBR25CLFFBQVEsQ0FBQy9DLE1BQU0sQ0FBQ21FLGdCQUFQLENBQXdCaEQsYUFBeEIsRUFBdUMsSUFBdkMsRUFBNkMsYUFBN0MsQ0FBRCxDQUF6QjtVQUNBLElBQU1pRCxXQUFXLEdBQUdyQixRQUFRLENBQUMvQyxNQUFNLENBQUNtRSxnQkFBUCxDQUF3QmhELGFBQXhCLEVBQXVDLElBQXZDLEVBQTZDLGdCQUE3QyxDQUFELENBQVIsR0FBMkUsQ0FBL0YsQ0FuQkQ7OztVQXNCQyxJQUFNUixRQUFRLEdBQUdtRCxRQUFRLENBQUNOLElBQUksQ0FBQ1EsR0FBTCxPQUFBUixJQUFJLHFCQUFRdEgsTUFBTSxDQUFDQyxJQUFQLENBQVkySCxRQUFaLENBQVIsRUFBTCxDQUF6Qjs7VUFDQSxJQUFJbkQsUUFBSixFQUFjOztZQUViLElBQUlBLFFBQVEsQ0FBQ3RHLFNBQVQsR0FBcUI4RyxhQUFhLENBQUNDLFNBQXZDLEVBQWtEO2NBQ2pERCxhQUFhLENBQUNDLFNBQWQsR0FBMEJULFFBQVEsQ0FBQ3RHLFNBQVQsR0FBcUI2SixRQUEvQzthQUREO2lCQUlLLElBQUl2RCxRQUFRLENBQUNQLFlBQVQsR0FBd0JPLFFBQVEsQ0FBQ3RHLFNBQWpDLEdBQTZDOEcsYUFBYSxDQUFDQyxTQUFkLEdBQTBCRCxhQUFhLENBQUNmLFlBQXpGLEVBQXVHO2NBQzNHZSxhQUFhLENBQUNDLFNBQWQsR0FBMEJULFFBQVEsQ0FBQ1AsWUFBVCxHQUF3Qk8sUUFBUSxDQUFDdEcsU0FBakMsR0FBNkM4RyxhQUFhLENBQUNmLFlBQTNELEdBQTBFZ0UsV0FBcEc7Ozs7O1FBS0hwRyxLQUFLLENBQUNpRCxRQUFELENBQUwsQ0FBZ0JJLFNBQWhCLEdBQTRCQSxTQUE1QjtRQUVBLElBQUlnRCx1QkFBSjtRQUNBbEQsYUFBYSxDQUFDUCxnQkFBZCxDQUErQixRQUEvQixFQUF5QyxVQUFDbkcsR0FBRCxFQUFTOztVQUVqRDZJLGlCQUFpQixHQUZnQzs7VUFJakRnQixZQUFZLENBQUNELHVCQUFELENBQVo7VUFDQUEsdUJBQXVCLEdBQUdFLFVBQVUsQ0FBQ2hCLGdCQUFELEVBQW1CLEdBQW5CLENBQXBDO1NBTEQ7UUFRQWxHLElBQUksQ0FBQ3VELGdCQUFMLENBQXNCLFNBQXRCLEVBQWlDLFVBQUNuRyxHQUFELEVBQVM7VUFDekMsSUFBSSxDQUFDLElBQUQsRUFBTyxLQUFQLEVBQWMrSixRQUFkLENBQXVCL0osR0FBRyxDQUFDb0csR0FBM0IsQ0FBSixFQUFxQztZQUNwQzs7O1VBRUQsSUFBTUEsR0FBRyxHQUFHcEcsR0FBRyxDQUFDb0csR0FBaEI7O1VBQ0EsSUFBSXBCLGFBQWEsTUFBTStDLGtCQUFrQixLQUFLdkIsUUFBOUMsRUFBd0Q7WUFDdkR4RyxHQUFHLENBQUNnSyxjQUFKOztZQUNBLElBQUloSyxHQUFHLENBQUNvRyxHQUFKLEtBQVksT0FBaEIsRUFBeUI7Y0FDeEJuQixjQUFjOzs7WUFFZixJQUFJakYsR0FBRyxDQUFDb0csR0FBSixLQUFZLFlBQWhCLEVBQThCO2NBQzdCK0IsZ0JBQWdCLENBQUMsTUFBRCxDQUFoQjs7O1lBRUQsSUFBSW5JLEdBQUcsQ0FBQ29HLEdBQUosS0FBWSxXQUFoQixFQUE2QjtjQUM1QitCLGdCQUFnQixDQUFDLE1BQUQsQ0FBaEI7OztZQUVELElBQUluSSxHQUFHLENBQUNvRyxHQUFKLEtBQVksS0FBaEIsRUFBdUI7Ozs7Y0FJdEIwRCxVQUFVLENBQUMsWUFBTTtnQkFDaEIzQixnQkFBZ0IsQ0FBQ0osa0JBQWtCLEtBQUssTUFBdkIsR0FBZ0MsTUFBaEMsR0FBeUMsTUFBMUMsQ0FBaEI7ZUFEUyxDQUFWOzs7WUFLRCxJQUFJLENBQUMsU0FBRCxFQUFZLFdBQVosRUFBeUJnQyxRQUF6QixDQUFrQzNELEdBQWxDLENBQUosRUFBNEM7Y0FDM0MsSUFBTTZELElBQUksR0FBR2pLLEdBQUcsQ0FBQ29HLEdBQUosS0FBWSxTQUFaLEdBQXdCLENBQUMsQ0FBekIsR0FBNkIsQ0FBMUM7Y0FDQSxJQUFNOEQsU0FBUyxHQUFHL0MsUUFBUSxDQUN6QixDQUFDbUIsUUFBUSxDQUFDeEIsb0JBQW9CLENBQUNOLFFBQUQsQ0FBckIsQ0FBUixHQUEyQ3lELElBQTNDLEdBQWtEeEcsS0FBbkQsSUFBNERBLEtBRG5DLENBQTFCO2NBR0FtRCxTQUFTLENBQUNzRCxTQUFELENBQVQsQ0FMMkM7OztZQVE1QyxJQUFJLENBQUMsU0FBRCxFQUFZLFdBQVosRUFBeUIsT0FBekIsRUFBa0NILFFBQWxDLENBQTJDM0QsR0FBM0MsQ0FBSixFQUFxRDtjQUNwRDRCLG9CQUFvQjs7O1NBbEN2QjtRQXVDQUgsTUFBTSxDQUFDMUcsT0FBUCxDQUFlLFVBQUEwRCxJQUFJLEVBQUk7VUFFdEJBLElBQUksQ0FBQ3NCLGdCQUFMLENBQXNCLFdBQXRCLEVBQW1DLFVBQUNuRyxHQUFELEVBQVM7WUFDM0MsSUFBSW9ELE1BQU0sQ0FBQ2QsdUJBQVgsRUFBb0M7Y0FDbkM2SCxnQkFBZ0IsR0FBR3BLLFFBQVEsQ0FBQ3FLLGFBQTVCOztXQUZGO1VBTUF2RixJQUFJLENBQUNzQixnQkFBTCxDQUFzQixPQUF0QixFQUErQixVQUFDbkcsR0FBRCxFQUFTO1lBQ3ZDLElBQU1zRCxHQUFHLEdBQUd0RCxHQUFHLENBQUNxSyxNQUFKLENBQVd0SSxPQUFYLENBQW1CSCxLQUEvQjtZQUNBLFdBQXFCLENBQUMwQyxDQUFDLENBQUNnRyxjQUFILEVBQW1CaEcsQ0FBQyxDQUFDaUcsWUFBckIsQ0FBckI7Z0JBQU83RyxLQUFQO2dCQUFjQyxHQUFkO1lBRUE4QyxXQUFXLENBQUNELFFBQUQsRUFBV2xELEdBQVgsQ0FBWDtZQUNBNkUsZ0JBQWdCLENBQUMzQixRQUFELENBQWhCO1lBQ0FJLFNBQVMsQ0FBQ3RELEdBQUQsQ0FBVDs7WUFDQSxJQUFJRixNQUFNLENBQUNkLHVCQUFYLEVBQW9DO2NBQ25DLElBQUk2SCxnQkFBZ0IsS0FBSzdGLENBQXpCLEVBQTRCO2dCQUMzQndGLFVBQVUsQ0FBQyxZQUFNO2tCQUNoQnhGLENBQUMsQ0FBQ2tHLEtBQUY7a0JBQ0FsRyxDQUFDLENBQUNtRyxpQkFBRixDQUFvQi9HLEtBQXBCLEVBQTJCQyxHQUEzQjtpQkFGUyxDQUFWOzs7V0FUSDtTQVJEO1FBMEJBc0YsY0FBYyxDQUFDbkMsb0JBQW9CLENBQUNOLFFBQUQsQ0FBckIsQ0FBZDtRQUNBMkIsZ0JBQWdCLENBQUMsTUFBRCxDQUFoQjtPQXZLRDtNQTBLQW5DLFlBQVksR0FBR00sZUFBZSxFQUE5Qjs7O0lBR0QxRCxJQUFJLENBQUN1RCxnQkFBTCxDQUFzQixPQUF0QixFQUErQixVQUFDbkcsR0FBRCxFQUFTO01BQ3ZDLElBQUksQ0FBQzBLLGtCQUFELElBQXVCMUYsYUFBYSxFQUF4QyxFQUE0QztRQUMzQyxJQUFNMkYsb0JBQW9CLEdBQUd6SSxDQUFDLENBQUM3QixZQUFGLENBQWVMLEdBQWYsRUFBb0I0SyxJQUFwQixDQUF5QixVQUFBL0YsSUFBSSxFQUFJO1VBQzdELE9BQU9BLElBQUksQ0FBQ3hELFNBQUwsSUFDSHdELElBQUksQ0FBQ3hELFNBQUwsQ0FBZXlELFFBQWYsQ0FBd0IsZ0JBQXhCLENBREo7U0FENEIsQ0FBN0I7O1FBSUEsSUFBSSxDQUFDNkYsb0JBQUwsRUFBMkI7VUFDMUIxRixjQUFjOzs7O01BR2hCeUYsa0JBQWtCLEdBQUcsS0FBckI7S0FWRDtJQWFBRyxJQUFJOztFQUlMO0VBQ0E7RUFDQTtFQUNBOztJQUVDLFNBQVNyRyxxQkFBVCxHQUFpQztNQUNoQyxJQUFJekQsTUFBTSxHQUFHbUIsQ0FBQyxDQUFDdEIsYUFBRixDQUFnQixNQUFoQixFQUF3QjtRQUFFLFNBQU87T0FBakMsQ0FBYjtNQUVBLElBQUlrSyxHQUFHLEdBQUc1SSxDQUFDLENBQUNGLGdCQUFGLENBQW1CLEtBQW5CLEVBQTBCO1FBQ25DLFdBQVcsYUFEd0I7UUFFbkMsU0FBUztPQUZBLENBQVY7TUFLQSxJQUFJK0ksTUFBTSxHQUFHN0ksQ0FBQyxDQUFDRixnQkFBRixDQUFtQixRQUFuQixFQUE2QjtRQUN6QyxNQUFNLElBRG1DO1FBQzdCLE1BQU0sSUFEdUI7UUFDakIsS0FBSyxJQURZO1FBRXpDLGdCQUFnQixHQUZ5QjtRQUVwQixVQUFVLE9BRlU7UUFFRCxnQkFBZ0I7T0FGNUMsQ0FBYjtNQUtBLElBQUlnSixLQUFLLEdBQUc5SSxDQUFDLENBQUNGLGdCQUFGLENBQW1CLE1BQW5CLEVBQTJCO1FBQ3RDLE1BQU0sSUFEZ0M7UUFDMUIsTUFBTSxJQURvQjtRQUNkLE1BQU0sSUFEUTtRQUNGLE1BQU0sSUFESjtRQUV0QyxVQUFVLE9BRjRCO1FBRW5CLGdCQUFnQjtPQUZ4QixDQUFaO01BS0EsSUFBSWlKLEtBQUssR0FBRy9JLENBQUMsQ0FBQ0YsZ0JBQUYsQ0FBbUIsTUFBbkIsRUFBMkI7UUFDdEMsTUFBTSxJQURnQztRQUMxQixNQUFNLElBRG9CO1FBQ2QsTUFBTSxJQURRO1FBQ0YsTUFBTSxJQURKO1FBRXRDLFVBQVUsT0FGNEI7UUFFbkIsZ0JBQWdCO09BRnhCLENBQVo7TUFLQWpCLE1BQU0sQ0FBQ3NELFdBQVAsQ0FBbUJ5RyxHQUFuQjtNQUNBQSxHQUFHLENBQUN6RyxXQUFKLENBQWdCMEcsTUFBaEI7TUFDQUQsR0FBRyxDQUFDekcsV0FBSixDQUFnQjJHLEtBQWhCO01BQ0FGLEdBQUcsQ0FBQ3pHLFdBQUosQ0FBZ0I0RyxLQUFoQjtNQUVBLE9BQU9sSyxNQUFQO0tBL1pGOzs7SUFtYUMsSUFBSTJDLEtBQUosRUFBV0MsR0FBWDtJQUVBLElBQUkrRyxrQkFBa0IsR0FBRyxLQUF6QjtJQUVBbkcsS0FBSyxDQUFDNEIsZ0JBQU4sQ0FBdUIsV0FBdkIsRUFBb0MsVUFBQ25HLEdBQUQsRUFBUztNQUM1QyxJQUFJb0QsTUFBTSxDQUFDZCx1QkFBWCxFQUFvQztRQUNuQzZILGdCQUFnQixHQUFHcEssUUFBUSxDQUFDcUssYUFBNUI7UUFEbUMsWUFFcEIsQ0FBQzlGLENBQUMsQ0FBQ2dHLGNBQUgsRUFBbUJoRyxDQUFDLENBQUNpRyxZQUFyQixDQUZvQjtRQUVsQzdHLEtBRmtDO1FBRTNCQyxHQUYyQjs7O01BSXBDLElBQUlQLE1BQU0sQ0FBQ2IsYUFBUCxLQUF5QixVQUE3QixFQUF5QzBDLGNBQWM7S0FMeEQ7SUFPQVYsS0FBSyxDQUFDNEIsZ0JBQU4sQ0FBdUIsT0FBdkIsRUFBZ0MsVUFBQ25HLEdBQUQsRUFBUztNQUN4QyxJQUFJLENBQUNnRixhQUFhLEVBQWxCLEVBQXNCO1FBQ3JCLElBQUk1QixNQUFNLENBQUNkLHVCQUFYLEVBQW9DO1VBQ25DZ0MsQ0FBQyxDQUFDa0csS0FBRjs7VUFDQSxJQUFJTCxnQkFBZ0IsS0FBSzdGLENBQXpCLEVBQTRCO1lBQzNCQSxDQUFDLENBQUNtRyxpQkFBRixDQUFvQi9HLEtBQXBCLEVBQTJCQyxHQUEzQjs7OztRQUdGK0csa0JBQWtCLEdBQUcsSUFBckI7UUFDQTNELFlBQVk7T0FSYixNQVNPO1FBQ04sSUFBSTNELE1BQU0sQ0FBQ2IsYUFBUCxLQUF5QixVQUE3QixFQUF5Q3dFLFlBQVk7UUFDckQsSUFBSTNELE1BQU0sQ0FBQ2IsYUFBUCxLQUF5QixXQUE3QixFQUEwQzBDLGNBQWM7O0tBWjFEOztFQWtCRDtFQUNBO0VBQ0E7RUFDQTs7SUFFQyxTQUFTNEYsSUFBVCxHQUFnQjtNQUNmLElBQUksQ0FBQ3ZHLENBQUMsQ0FBQzFDLEtBQVAsRUFBYztRQUNiMEMsQ0FBQyxDQUFDMUMsS0FBRixHQUFVLE9BQVY7Ozs7SUFJRixTQUFTaUYsV0FBVCxDQUFxQnVCLFFBQXJCLEVBQStCO01BQzlCLE9BQU85RCxDQUFDLENBQUMxQyxLQUFGLENBQVFzSixTQUFSLENBQWtCM0gsS0FBSyxDQUFDNkUsUUFBRCxDQUFMLENBQWdCMUUsS0FBbEMsRUFBeUNILEtBQUssQ0FBQzZFLFFBQUQsQ0FBTCxDQUFnQnpFLEdBQXpELENBQVA7OztJQUdELFNBQVN3SCxPQUFULEdBQW1CO01BQUUsT0FBT3RFLFdBQVcsQ0FBQyxNQUFELENBQWxCOzs7SUFFckIsU0FBU3VFLGFBQVQsQ0FBdUJoRCxRQUF2QixFQUFpQztNQUNoQyxPQUFPN0UsS0FBSyxDQUFDNkUsUUFBRCxDQUFMLENBQWdCeEUsT0FBaEIsQ0FBd0J5SCxJQUF4QixDQUE2QixJQUE3QixDQUFQOzs7SUFLRCxTQUFTdkUsb0JBQVQsQ0FBOEJzQixRQUE5QixFQUF3QztNQUN2QyxJQUFNa0QsZUFBZSxHQUFHN0osTUFBTSxDQUFDQyxJQUFQLENBQVk2QixLQUFaLEVBQ3RCcUIsTUFEc0IsQ0FDZixVQUFBd0QsUUFBUTtRQUFBLE9BQUltRCxLQUFLLENBQUNqRCxRQUFRLENBQUN6QixXQUFXLENBQUN1QixRQUFELENBQVosQ0FBVCxDQUFUO09BRE8sRUFFdEJHLE1BRnNCLEdBRWIsQ0FGWDtNQUlBLE9BQU8rQyxlQUFlLEdBQ25CbkUsUUFBUSxDQUFDaUUsYUFBYSxDQUFDaEQsUUFBRCxDQUFkLENBRFcsR0FFbkJ2QixXQUFXLENBQUN1QixRQUFELENBRmQ7OztJQU9ELFNBQVNvRCxjQUFULENBQXdCcEQsUUFBeEIsRUFBa0M7TUFDakM5RCxDQUFDLENBQUNtRyxpQkFBRixDQUFvQmxILEtBQUssQ0FBQzZFLFFBQUQsQ0FBTCxDQUFnQjFFLEtBQXBDLEVBQTJDSCxLQUFLLENBQUM2RSxRQUFELENBQUwsQ0FBZ0J6RSxHQUEzRDs7O0lBS0QsU0FBUzhILGFBQVQsQ0FBdUJyRCxRQUF2QixFQUFpQztNQUNoQ3NELFVBQVUsR0FBRyxJQUFiO01BQ0FDLFNBQVMsR0FBR3ZELFFBQVo7TUFDQW9ELGNBQWMsQ0FBQ3BELFFBQUQsQ0FBZDs7O0lBRUQsU0FBU3dELFNBQVQsR0FBcUI7TUFBRSxPQUFPSCxhQUFhLENBQUMsTUFBRCxDQUFwQjs7O0lBQ3ZCLFNBQVNJLFNBQVQsR0FBcUI7TUFBRSxPQUFPSixhQUFhLENBQUMsTUFBRCxDQUFwQjs7O0lBRXZCLFNBQVN0RSxRQUFULENBQWtCN0QsR0FBbEIsRUFBOEI7TUFBQSxJQUFQaUUsQ0FBTyx1RUFBSCxDQUFHO01BQzdCLE9BQU91RSxNQUFNLENBQUN4SSxHQUFELENBQU4sQ0FBWTRILFNBQVosQ0FBc0I1SCxHQUFHLENBQUNpRixNQUFKLEdBQWFoQixDQUFuQyxFQUFzQ3dFLFFBQXRDLENBQStDeEUsQ0FBL0MsRUFBa0QsR0FBbEQsQ0FBUDs7O0lBR0QsU0FBU2QsV0FBVCxDQUFxQjJCLFFBQXJCLEVBQStCOUUsR0FBL0IsRUFBb0M7TUFDbkMsWUFBcUIsQ0FBQ0MsS0FBSyxDQUFDNkUsUUFBRCxDQUFMLENBQWdCMUUsS0FBakIsRUFBd0JILEtBQUssQ0FBQzZFLFFBQUQsQ0FBTCxDQUFnQnpFLEdBQXhDLENBQXJCO1VBQU9ELEtBQVA7VUFBY0MsR0FBZDtNQUNBVyxDQUFDLENBQUMxQyxLQUFGLEdBQVUwQyxDQUFDLENBQUMxQyxLQUFGLENBQVFzSixTQUFSLENBQWtCLENBQWxCLEVBQXFCeEgsS0FBckIsSUFDSnlELFFBQVEsQ0FBQzdELEdBQUQsQ0FESixHQUVKZ0IsQ0FBQyxDQUFDMUMsS0FBRixDQUFRc0osU0FBUixDQUFrQnZILEdBQWxCLENBRk47TUFHQTZILGNBQWMsQ0FBQ3BELFFBQUQsQ0FBZDtNQUVBMUYsVUFBVSxDQUFDZCxLQUFYLEdBQW1Cb0ssWUFBWSxDQUFDMUgsQ0FBQyxDQUFDMUMsS0FBSCxDQUEvQjtNQUNBTSxDQUFDLENBQUNyQyxZQUFGLENBQWU2QyxVQUFmLEVBQTJCLFFBQTNCOzs7SUFHRCxTQUFTdUosT0FBVCxDQUFpQjNJLEdBQWpCLEVBQXNCO01BQUVtRCxXQUFXLENBQUMsTUFBRCxFQUFTbkQsR0FBVCxDQUFYOzs7SUFFeEIsU0FBUzBJLFlBQVQsQ0FBc0JwSyxLQUF0QixFQUE2QjtNQUM1QixPQUFPQSxLQUFLLENBQUNtSSxRQUFOLENBQWUsR0FBZixJQUFzQixJQUF0QixHQUE2Qm5JLEtBQXBDOzs7SUFHRCxJQUFJOEosVUFBVSxHQUFHLEtBQWpCO0lBQ0EsSUFBSUMsU0FBUyxHQUFHLElBQWhCO0lBRUEsSUFBSXhCLGdCQUFnQixHQUFHLElBQXZCLENBemdCRDs7SUE0Z0JDLENBQUMsT0FBRCxFQUFVLFdBQVYsRUFBdUIsTUFBdkIsRUFBK0JoSixPQUEvQixDQUF1QyxVQUFDckIsT0FBRCxFQUFhO01BQ25Ed0UsQ0FBQyxDQUFDNkIsZ0JBQUYsQ0FBbUJyRyxPQUFuQixFQUE0QixVQUFDRSxHQUFELEVBQVM7UUFBRUEsR0FBRyxDQUFDZ0ssY0FBSjtPQUF2QztLQUREO0lBSUExRixDQUFDLENBQUM2QixnQkFBRixDQUFtQixPQUFuQixFQUE0QixVQUFDbkcsR0FBRCxFQUFTO01BQ3BDQSxHQUFHLENBQUNnSyxjQUFKO01BQ0E0QixTQUFTO0tBRlY7SUFJQXRILENBQUMsQ0FBQzZCLGdCQUFGLENBQW1CLE9BQW5CLEVBQTRCL0MsTUFBTSxDQUFDRCxPQUFuQztJQUVBbUIsQ0FBQyxDQUFDNkIsZ0JBQUYsQ0FBbUIsVUFBbkIsRUFBK0IsVUFBQ25HLEdBQUQsRUFBUzs7TUFFdkMsSUFBSStELElBQUksR0FBR29ILE9BQU8sRUFBbEI7O01BQ0EsSUFBSSxDQUFDSSxLQUFLLENBQUN4SCxJQUFELENBQU4sSUFBZ0JBLElBQUksSUFBSVgsTUFBTSxDQUFDSCxPQUFuQyxFQUE0QztRQUMzQ2dKLE9BQU8sQ0FBQzdJLE1BQU0sQ0FBQ0gsT0FBUCxHQUFpQixDQUFsQixDQUFQOztLQUpGO0lBT0FxQixDQUFDLENBQUM2QixnQkFBRixDQUFtQixVQUFuQixFQUErQi9DLE1BQU0sQ0FBQzhJLE1BQXRDO0lBRUE1SCxDQUFDLENBQUM2QixnQkFBRixDQUFtQixXQUFuQixFQUFnQyxVQUFDbkcsR0FBRCxFQUFTO0tBQXpDO0lBSUFzRSxDQUFDLENBQUM2QixnQkFBRixDQUFtQixTQUFuQixFQUE4QixVQUFDbkcsR0FBRCxFQUFTOztNQUV0QyxJQUFJLENBQUMsSUFBRCxFQUFPLEtBQVAsRUFBYytKLFFBQWQsQ0FBdUIvSixHQUFHLENBQUNvRyxHQUEzQixDQUFKLEVBQXFDO1FBQUU7OztNQUV2QyxJQUFNSSxRQUFRLEdBQUdsQyxDQUFDLENBQUNnRyxjQUFGLEtBQXFCLENBQXJCLEdBQXlCLE1BQXpCLEdBQWtDLE1BQW5ELENBSnNDOztNQU90QyxJQUFJdEssR0FBRyxDQUFDb0csR0FBSixLQUFZLEtBQVosSUFBcUIsQ0FBQ3BHLEdBQUcsQ0FBQ21NLFFBQTFCLElBQXNDM0YsUUFBUSxLQUFLLE1BQXZELEVBQStEO1FBQUU7OztNQUNqRSxJQUFJeEcsR0FBRyxDQUFDb0csR0FBSixLQUFZLEtBQVosSUFBcUJwRyxHQUFHLENBQUNtTSxRQUF6QixJQUFxQzNGLFFBQVEsS0FBSyxNQUF0RCxFQUE4RDtRQUFFOzs7TUFFaEV4RyxHQUFHLENBQUNnSyxjQUFKLEdBVnNDOzs7TUFhdEMsSUFBSWhGLGFBQWEsRUFBakIsRUFBcUI7UUFDcEIsSUFBSSxDQUFDLFlBQUQsRUFBZSxXQUFmLEVBQTRCLFNBQTVCLEVBQXVDLFdBQXZDLEVBQW9ELE9BQXBELEVBQTZEK0UsUUFBN0QsQ0FBc0UvSixHQUFHLENBQUNvRyxHQUExRSxDQUFKLEVBQW9GO1VBQ25GOzs7O01BSUYsSUFBSXBHLEdBQUcsQ0FBQ29HLEdBQUosS0FBWSxLQUFaLElBQXFCcEcsR0FBRyxDQUFDbU0sUUFBekIsSUFBcUMzRixRQUFRLEtBQUssTUFBdEQsRUFBOEQ7UUFDN0QsT0FBT29GLFNBQVMsRUFBaEI7OztNQUVELElBQUk1TCxHQUFHLENBQUNvRyxHQUFKLEtBQVksS0FBWixJQUFxQixDQUFDcEcsR0FBRyxDQUFDbU0sUUFBMUIsSUFBc0MzRixRQUFRLEtBQUssTUFBdkQsRUFBK0Q7UUFDOUQsT0FBT3FGLFNBQVMsRUFBaEI7OztNQUdELElBQUk3TCxHQUFHLENBQUNvRyxHQUFKLEtBQVksWUFBaEIsRUFBOEI7UUFDN0IsT0FBT3lGLFNBQVMsRUFBaEI7OztNQUVELElBQUk3TCxHQUFHLENBQUNvRyxHQUFKLEtBQVksV0FBaEIsRUFBNkI7UUFDNUIsT0FBT3dGLFNBQVMsRUFBaEI7OztNQUdELElBQU1oSyxLQUFLLEdBQUcwQyxDQUFDLENBQUMxQyxLQUFoQjtNQUNBLElBQU13SyxTQUFTLEdBQUd2RixXQUFXLENBQUNMLFFBQUQsQ0FBN0I7TUFDQSxJQUFNL0MsS0FBSyxHQUFHa0ksU0FBUyxLQUFLLE1BQWQsR0FBdUJ2SSxNQUFNLENBQUNKLEtBQTlCLEdBQXNDSSxNQUFNLENBQUNILE9BQTNEO01BQ0EsSUFBSWlILFNBQVMsR0FBRyxFQUFoQjs7TUFFQSxJQUFJLENBQUMsU0FBRCxFQUFZLFdBQVosRUFBeUJILFFBQXpCLENBQWtDL0osR0FBRyxDQUFDb0csR0FBdEMsQ0FBSixFQUFnRDtRQUMvQyxJQUFNNkQsSUFBSSxHQUFHakssR0FBRyxDQUFDb0csR0FBSixLQUFZLFNBQVosR0FBd0IsQ0FBeEIsR0FBNEIsQ0FBQyxDQUExQztRQUVBOEQsU0FBUyxHQUFHL0MsUUFBUSxDQUNuQm9FLEtBQUssQ0FBQ2pELFFBQVEsQ0FBQzhELFNBQUQsQ0FBVCxDQUFMLEdBQ0csQ0FESCxHQUNPLENBQUM5RCxRQUFRLENBQUM4RCxTQUFELENBQVIsR0FBc0JuQyxJQUF0QixHQUE2QnhHLEtBQTlCLElBQXVDQSxLQUYzQixDQUFwQjs7O01BTUQsSUFBSSxDQUFDLFdBQUQsRUFBYyxRQUFkLEVBQXdCc0csUUFBeEIsQ0FBaUMvSixHQUFHLENBQUNvRyxHQUFyQyxDQUFKLEVBQStDO1FBQzlDOEQsU0FBUyxHQUFHLElBQVo7O1FBQ0EsSUFBSXlCLFNBQVMsS0FBSyxNQUFsQixFQUEwQjtVQUN6QkMsU0FBUzs7OztNQUlYLElBQUk1TCxHQUFHLENBQUNvRyxHQUFKLENBQVFpRyxLQUFSLENBQWMsU0FBZCxDQUFKLEVBQ0E7UUFDQ25DLFNBQVMsR0FBRyxDQUFDd0IsVUFBVSxHQUFHLEdBQUgsR0FBU1UsU0FBUyxDQUFDbEIsU0FBVixDQUFvQixDQUFwQixDQUFwQixJQUE4Q2xMLEdBQUcsQ0FBQ29HLEdBQTlEOztRQUVBLElBQUl1RixTQUFTLEtBQUssTUFBbEIsRUFBMEI7O1VBRXpCLElBQUlELFVBQVUsSUFBSXBELFFBQVEsQ0FBQzRCLFNBQUQsQ0FBUixHQUFzQixFQUF0QixJQUE0QnpHLEtBQTlDLEVBQXFEO1lBQ3BEcUcsVUFBVSxDQUFDK0IsU0FBRCxDQUFWOzs7VUFFRCxJQUFJLENBQUNILFVBQUwsRUFBaUI7O1lBRWhCLElBQUlwRCxRQUFRLENBQUM0QixTQUFELENBQVIsSUFBdUJ6RyxLQUEzQixFQUFrQztjQUNqQ3lHLFNBQVMsR0FBRzRCLE1BQU0sQ0FBQ3JJLEtBQUssR0FBRyxDQUFULENBQWxCOzs7WUFFRHFHLFVBQVUsQ0FBQytCLFNBQUQsQ0FBVjs7OztRQUdGSCxVQUFVLEdBQUcsS0FBYjs7O01BR0QsSUFBSXhCLFNBQUosRUFBZTtRQUNkekQsV0FBVyxDQUFDRCxRQUFELEVBQVcwRCxTQUFYLENBQVg7O0tBM0VGO0lBK0VBNUYsQ0FBQyxDQUFDNkIsZ0JBQUYsQ0FBbUIsU0FBbkIsRUFBOEIsVUFBQ25HLEdBQUQsRUFBUztNQUN0Q0EsR0FBRyxDQUFDZ0ssY0FBSjs7TUFDQSxJQUFJMUYsQ0FBQyxDQUFDZ0csY0FBRixJQUFvQixDQUF4QixFQUEyQjtRQUMxQnVCLFNBQVM7T0FEVixNQUVPO1FBQ05ELFNBQVM7O0tBTFg7SUFTQWxKLFVBQVUsQ0FBQ3lELGdCQUFYLENBQTRCLFFBQTVCLEVBQXNDL0MsTUFBTSxDQUFDRixRQUE3QztJQUVBLE9BQU87TUFDTm9KLFlBQVksRUFBRXJJLGFBRFI7TUFFTnVHLEtBQUssRUFBRSxpQkFBTTtRQUFFdEcsZUFBZSxDQUFDc0csS0FBaEI7O0tBRmhCO0VBSUEsQ0Fsb0JEOzs7Ozs7OzsifQ==
